import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, LoadingController } from 'ionic-angular';
import { AppointmentListPage } from '../appointmentList/appointmentList';
import { AccountServiceProvider } from '../../providers/account-service/account-service';
import { FormGroup, FormBuilder } from '@angular/forms';
import moment from 'moment';

@IonicPage()
@Component({
  selector: 'page-appointment',
  templateUrl: 'appointmentCreate.html',
})
export class AppointmentPage {
  car: any;
  carArray: any[] = [];
  carList: any[] = [];
  cars: any[] = [];
  myForm: FormGroup;
  email: any;
  username: string;
  usersList: any[] = [];
  users: any[] = [];
  usersArray: any[] = [];
  promo:any;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public accountService: AccountServiceProvider,
    public formBuilder: FormBuilder
  ) {
    
    this.promo=navParams.get('promo');
    console.log(this.promo);
    this.myForm = this.createMyForm();

  }

  loadUserCredentialsStorage(): string {
    var username = window.localStorage.getItem('user');
    return username;
  }



  ionViewWillEnter() {
    this.presentLoading();
  }

  public createMyForm() {
    return this.formBuilder.group({
      car: [''],
      fecha: [''],
      hora: [''],
      tipo: ['']
    });
  }

  //Spinner between two screens
  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 300
    });
    //after charge, get the Cars
    this.username = this.loadUserCredentialsStorage();
    this.carList = this.carArray;
    this.usersList = this.usersArray;
    this.getUsers();
    this.getCar();
    loader.present();
  }

  doRefresh(refresher) {
    this.getUsers().then(() => {
      refresher.complete();
    });
  }

  setAppointment() {
    let car = this.myForm.get('car').value;
    let fecha = (this.myForm.get('fecha').value).toString();
    let date = moment(fecha).format('DD-MM-YYYY');
    let hours = (this.myForm.get('hora').value).toString();
    let tipo = this.myForm.get('tipo').value;
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 300
    });
    loader.present().then(() => {
      this.accountService.setAppointment(hours, date, tipo, car,this.promo,this.username)
        .then(data => {
          if (data) {
            var alert = this.alertCtrl.create({
              title: "Success",
              subTitle: "Appointment created correctly",
              buttons: ['OK']
            });
            alert.present();
          }
        });
      this.myForm.reset();
      this.navCtrl.parent.select(2);
    });
  }
  initializeCars() {
    this.cars = this.carList;
  }

  getItems(ev: any) {
    this.initializeUsers();

    // set val to the value of the searchbar
    let val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.users = this.users.filter((item) => { //remember that the item have all the fields of a city Object
        //With item.name we choose the city name that is a String
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }


  getCar() {
    return new Promise((resolve) => {
      let carsArray = [];
      this.accountService.getCarsService()
        .subscribe((data) => {
          let cars = data.json();
          if (cars.length != 0) {
            for (var i of cars) {
              carsArray.push(i);
            }
            this.carList = carsArray;
            this.initializeCars();
          }
        });
      resolve(true);
    });
  }

  initializeUsers() {
    this.users = this.usersList;
  }


  getUsers() {
    return new Promise((resolve) => {
      let usersArray = [];
      this.accountService.getListUsers()
        .subscribe((data) => {
          let users = data.json();
          if (users.length != 0) {
            for (var i of users) {
              usersArray.push(i);
            }
            this.usersList = usersArray;
            this.initializeUsers();
          }
        });
      resolve(true);
    });
  }

  getUser(username:string) {
    return new Promise((resolve) => {
      let usersArray = [];
      this.accountService.getUser(username)
        .subscribe((data) => {
          let users = data.json();
          if (users.length != 0) {
            for (var i of users) {
              usersArray.push(i);
            }
            this.usersList = usersArray;
            this.initializeUsers();
          }
        });
      resolve(true);
    });
  }


  deleteUser(email: string) {
    var alert: any;
    this.accountService.deleteUser(email)
      .subscribe((data) => {
        if (data) {
          alert = this.alertCtrl.create({
            title: "HECHO",
            subTitle: "Usuario de forma correcta",
            buttons: ['OK']
          });
          this.getUsers();
          alert.present();
        }
        else {
          alert = this.alertCtrl.create({
            title: 'Error',
            subTitle: "No se ha podido eliminar el usuario",
            buttons: ['OK']
          });
          alert.present();
        }
      });
  }
}
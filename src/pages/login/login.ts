import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { AlertController} from "ionic-angular";
import { SignupPage } from '../signup/signup';
import { AccountServiceProvider } from '../../providers/account-service/account-service';
import { TabsPage } from '../tabs/tabs';
import { ForgotPasswordPage } from '../forgot-password/forgot-password';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {

  user: string = "";
  password: string = "";
  isLoggedin: boolean;
  AuthToken;

  account = { user: '', password: '' };

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    public accountService: AccountServiceProvider
  ) {

  } 

  //Login with email and password
  login() {
    this.accountService.loginService(this.account.user, this.account.password).then((res) => {
      if (res) {
        this.storeUserCredentials(this.account.user);
        this.navCtrl.push(TabsPage);
        
      }
      else {
        var alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: "Try again",
          buttons: ['OK']
        });
        alert.present();
      }
    });;
  }

  

  useCredentials(user) {
    this.isLoggedin = true;
    this.AuthToken = user;
  }
  storeUserCredentials(user) {
    //sessionstorage???
    window.localStorage.setItem('user', user);
    this.useCredentials(user);
  }

  /* Voy a la página de sign up para rellenar el formulario*/
    /*Con este push, si quiero volver atrás (página inicial) con un pop*/
  toSingUp() { 
    this.navCtrl.push(SignupPage);
  }

  gotoforgotpassword() {
    this.navCtrl.push(ForgotPasswordPage);
  }

}
  
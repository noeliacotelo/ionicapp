import { Component } from '@angular/core';
import { ListCarsPage } from '../cars/cars';
import {AppointmentListPage} from '../appointmentList/appointmentList'
import { ProfilePage } from '../profile/profile';
import {AppointmentPage} from '../appointmentCreate/appointmentCreate';
import { NavParams, NavController } from 'ionic-angular';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root : any;
  tab2Root : any;
  tab3Root : any;
  tab4Root : any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.tab1Root = ListCarsPage;
    this.tab2Root = AppointmentPage;
    this.tab3Root = AppointmentListPage;
    this.tab4Root = ProfilePage;
  }
}

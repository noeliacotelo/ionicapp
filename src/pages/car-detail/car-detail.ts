import { Component} from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { AccountServiceProvider } from '../../providers/account-service/account-service';
import { FormBuilder } from '@angular/forms';
import { Http } from '@angular/http';
import { ListCarsPage } from '../cars/cars';
import { TranslateLoader } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-car-detail',
  templateUrl: 'car-detail.html',
})
export class CarDetailPage {
  car: any;
  username: string;

  constructor(public navCtrl: NavController,
    public http: Http,
    public navParams: NavParams,
    public accountServiceProvider: AccountServiceProvider,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public formBuilder: FormBuilder) {

    this.car = navParams.get('car'); //Get the car from the previous page
  }

  ionViewWillEnter() {
    this.presentLoading();
  }

  loadUserCredentialsStorage(): string {
    var username = window.localStorage.getItem('user');
    return username;
  }
  //Spinner between two screens
  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 300
    });
    //after charge, get the cars and initialize the list
    this.username = this.loadUserCredentialsStorage();
    loader.present();
  }


  deleteCar(carId: any) {
    var alert: any;
    this.accountServiceProvider.deleteCar(carId)
      .subscribe((data) => {
        if (data) {
          alert = this.alertCtrl.create({
            title: "HECHO",
            subTitle: "Coche eliminado de forma correcta",
            buttons: ['OK']
          });
          this.navCtrl.setRoot(ListCarsPage);
          alert.present();
        }
        else {
          alert = this.alertCtrl.create({
            title: 'Error',
            subTitle: "No se ha podido eliminar el coche seleccionado",
            buttons: ['OK']
          });
          alert.present();
        }
      });
  }


  
}

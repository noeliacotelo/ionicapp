import { Component } from '@angular/core';
import { IonicPage, NavController} from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core'
import { TabsPage } from '../tabs/tabs';

@IonicPage()
@Component({
  selector: 'page-language',
  templateUrl: 'language.html',
})
export class LanguagePage {

  languages: any[] = [];

  constructor(
    private navCtrl: NavController,
    private translateService: TranslateService
  ) {
    this.languages = [
      {
        value: 'es',
        label: 'Español',
        img: 'assets/icon/spain.ico'
      },
      {
        value: 'en',
        label: 'Inglés',
        img: 'assets/icon/uk.ico'
      }
    ];
  }

  choose(lang) {
    this.translateService.use(lang);
    this.navCtrl.setRoot(TabsPage);
  }

  ionViewDidLoad() {
  }

}

import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CarCreatePage } from './carcreate';

@NgModule({
  declarations: [
    CarCreatePage,
  ],
  imports: [
    IonicPageModule.forChild(CarCreatePage),
  ],
})

export class CarcreatePageModule {
}

import { Component } from '@angular/core';
import {FormBuilder, FormGroup,FormControl, Validators } from '@angular/forms';
import { IonicPage, NavController,ViewController,LoadingController, NavParams } from 'ionic-angular';
import { AlertController } from "ionic-angular";
import {AccountServiceProvider} from '../../providers/account-service/account-service'
import {ListCarsPage } from '../cars/cars';

@IonicPage()
@Component({
  selector: 'page-car-create',
  templateUrl: 'carcreate.html',
})
export class CarCreatePage {
  myForm:FormGroup;
  validations_form : FormGroup;
  username:string;



  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController, 
    public formBuilder: FormBuilder,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public accountService : AccountServiceProvider) {

      this.myForm = this.createMyForm();
     // Watch the form for changes, and
  }

  loadUserCredentialsStorage(): string {
    var username = window.localStorage.getItem('user');
    return username;
  }

  ionViewWillEnter() {
    this.presentLoading();
  }
  
public createMyForm() {
  return this.formBuilder.group({
    marca: [''],
    matricula: new FormControl('', [Validators.required, Validators.minLength(7),Validators.maxLength(7)]),
    color: [''],
    km: [''],
    potencia: [''],
    ano: [''],
    combustible: ['']
  });
}


//Spinner between two screens
presentLoading() {
  let loader = this.loadingCtrl.create({
    content: "Please wait...",
    duration: 30
  });
  //after charge, get the cars and initialize the list
  this.username=this.loadUserCredentialsStorage();
  loader.present();
}

saveData(){
  let name:string = this.myForm.get('marca').value;
  let matricula:string = this.myForm.get('matricula').value;
  let km:number = this.myForm.get('km').value;
  let ano:number = this.myForm.get('ano').value;
  let combustible:string = this.myForm.get('combustible').value;
  let color:string = (this.myForm.get('color').value).toString();
  let potencia:number = this.myForm.get('potencia').value;

  let loader = this.loadingCtrl.create({
    content: "Please wait...",
    duration: 30
  });
  loader.present().then(() => {
    this.accountService.setCars(this.username,name, matricula, km, ano, combustible, color, potencia)
      .then(data => {
        if (data) {
          var alert = this.alertCtrl.create({
            title: "Success",
            subTitle: "Car created correctly",
            buttons: ['OK']
          });
            alert.present();
            this.navCtrl.setRoot(ListCarsPage);
        } else { var alert = this.alertCtrl.create({
          title: "Success",
          subTitle: "Car created correctly",
          buttons: ['OK']
        });
          alert.present();}
    });


});
}


cancel() {
  this.viewCtrl.dismiss();
}



}
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController, LoadingController } from 'ionic-angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AccountServiceProvider } from '../../providers/account-service/account-service';
import { ListCarsPage } from '../cars/cars';
import { AppointmentListPage } from '../appointmentList/appointmentList';

/**
 * Generated class for the BillPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bill',
  templateUrl: 'bill.html',
})
export class BillPage {
  myForm: FormGroup;
  appointment: any;
  user:any;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public formBuilder: FormBuilder,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public accountService: AccountServiceProvider) {
    this.myForm = this.createMyForm();
    this.appointment = navParams.get('appointment');
    
  }

  ionViewDidLoad() {
    this.presentLoading();
  }

  public createMyForm(){
    return this.formBuilder.group({
      descripcion:[''],
      importe:[''],
      iva:[''],
      total:[''],
      cantidad:['']
    });
  }

  //Spinner between two screens
presentLoading() {
  let loader = this.loadingCtrl.create({
    content: "Please wait...",
    duration: 30
  });
  //after charge, get the cars and initialize the list
  loader.dismiss();
}

saveData(appointmentId:any, date:any){
  let descripcion= this.myForm.get('descripcion').value;
  let importe = this.myForm.get('importe').value;
  let cantidad = this.myForm.get('cantidad').value;
  let cif = "48112863Z";

  let loader = this.loadingCtrl.create({
    content: "Please wait...",
    duration: 30
  });
  loader.present().then(() => {
    this.accountService.setBill(descripcion,importe,cantidad,appointmentId, date,cif)
      .then(data => {
        if (data) {
          var alert = this.alertCtrl.create({
            title: "HECHO",
            subTitle: "Factura creada correctamente",
            buttons: ['OK']
          });
          alert.present();
        }
      });
    this.myForm.reset();
    this.navCtrl.setRoot(AppointmentListPage);

});
}
cancel() {
  this.viewCtrl.dismiss();
}

}

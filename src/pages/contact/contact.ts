import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { EmailComposer } from '@ionic-native/email-composer';

/**
 * Generated class for the ContactPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html',
})
export class ContactPage {

  to: '';
  body: '';
  subject: 'Soporte aplicacion y serivicios'
  
  constructor(
    public navCtrl: NavController,
    public emailComposer:EmailComposer
  ) { 

  }
  
  send(){

    this.emailComposer.isAvailable().then((available: boolean) =>{ 
      if(available) { 
          console.log('posible')
      } else {
          console.log('imposible')
      }
    });

    let emailSupport = {
      to: 'talleresantonio@gmail.com',
      cc: [],
      bcc: [],
      attachment: [],
      subject: this.subject,
      body: this.body,
      isHtml: false
    }

    let emailRecibido = {
      to: this.to,
      cc: [],
      bcc: [],
      attachment: [],
      subject: 'Ha contactado con support',
      body: ' En breve nos pondremos en contacto con usted',
      isHtml: false
    }   

    this.emailComposer.open(emailSupport);
    this.emailComposer.open(emailRecibido);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactPage');
  }

}

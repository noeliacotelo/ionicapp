import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { LoginPage } from '../login/login';

/**
 * Generated class for the ForgotPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {
 

  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    public alertCtrl: AlertController,) {
  }


  ionViewDidLoad() {
  }

  resetPassword() {
    var alert = this.alertCtrl.create({
      title: 'Solicitud recibida',
      subTitle: "En breve recibirá un correo con la información para su acceso",
      buttons: ['OK']
    });
    alert.present();
    
    this.navCtrl.setRoot(LoginPage);
  }
}

import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ViewController, AlertController, Nav } from 'ionic-angular';
import { AccountServiceProvider } from '../../providers/account-service/account-service';
import { LoginPage } from '../login/login';
import { AppointmentPage } from '../appointmentCreate/appointmentCreate';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})

export class ProfilePage {
name: string = "";
email: string = "";
promos: any[] = [];
promosArray: any[] =[];
promosList: any[] = [];
users: string;
user:string;
username:string;


  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public accountService: AccountServiceProvider,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public nav: Nav
    ) {
  }

  loadUserCredentialsStorage(): string {
    var username = window.localStorage.getItem('user');
    return username;
  }

  ionViewWillEnter() {
    this.presentLoading();
  }
 
  presentLoading(){
    let loader = this.loadingCtrl.create({
      content:"Please wait....",
      duration: 100
    });
    //after charge, get the promos and intialize the list 
    loader.present();
    this.username= this.loadUserCredentialsStorage();
    this.promosList = this.promosArray;
      this.getPromos();
  }

  initializePromos() {
    this.promos=this.promosList;
  }

  setPromo(promo:any){
    this.navCtrl.parent.select(1,).then(() =>{
    this.navCtrl.parent.getSelected().push(AppointmentPage, {promo:promo});
});
  
  }

  getPromos() {
    return new Promise((resolve) => {
      let promosArray=[];
      this.accountService.getPromosService()
        .subscribe((data) => {
          let promociones = data.json();
          if (promociones.length != 0) {
            for (var i of promociones) {
              promosArray.push(i);
            }
              this.promosList = promosArray;
              this.initializePromos();
            }
          });
        resolve(true);
      });
  }

  initializeUser(){
    this.users=this.user
  }


  logout() {
    // this.menuCtrl.close();
    this.loaderOut();
     this.nav.setRoot(LoginPage);

   }
  loaderOut() {
    let loader = this.loadingCtrl.create({
      content:"Please wait....",
      duration: 100
    });
    //after charge, get the promos and intialize the list 
    loader.present();
  }

}

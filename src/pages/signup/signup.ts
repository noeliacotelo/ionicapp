import { Component } from '@angular/core';
import { NavController, NavParams, ViewController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { AccountServiceProvider } from '../../providers/account-service/account-service';
import { AlertController} from "ionic-angular";


@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  name: string = "";
  email: string = "";
  password: string = "";
  isLoggedin: boolean;
  AuthToken;

  account={name: '', email:'', password: ''};

  constructor(public navCtrl: NavController,
  public navParams: NavParams,
  public viewCtrl: ViewController,
  public alertCtrl: AlertController,
  public accountService: AccountServiceProvider
  ){}
  

  signup() {
    this.accountService.signupService(this.account.email, this.account.name, this.account.password).then
    ((res)=>{
      if(res){
        this.storeUserCredentials(this.account.email)
        this.navCtrl.setRoot(TabsPage);
      }
      else{
        var alert = this.alertCtrl.create({
          title: 'Error',
          subTitle: "Try again",
          buttons: ['OK']
        });
        alert.present();
      }
    });
  }

  back(){
    this.navCtrl.pop();
  }

  useCredentials(user) {
    this.isLoggedin = true;
    this.AuthToken = user;
  }
  storeUserCredentials(user) {
    //sessionstorage???
    window.localStorage.setItem('user', user);
    this.useCredentials(user);
  }

}

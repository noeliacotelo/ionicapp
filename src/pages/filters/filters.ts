import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { AccountServiceProvider } from '../../providers/account-service/account-service';
import { Events } from 'ionic-angular';
/**
 * Generated class for the FiltersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-filters',
  templateUrl: 'filters.html',
})
export class FiltersPage {

  state:any;
  tipo:any;
  states: string[] = ["Pendiente","En curso", "Finalizado","Cancelado"];
  tipos: string[] = ["Mantenimiento","Reparacion"];

  constructor(public navCtrl: NavController,
    public http: Http,
    public navParams: NavParams,
    public accountService: AccountServiceProvider,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public events:Events) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FiltersPage');
  }

  setState(state: any){
    this.state = state;
  }

  setTipo(tipo:any){
    this.tipo=tipo;
  }

  apply(){
    this.events.publish('state:tipo', this.state, this.tipo);
    this.navCtrl.pop();
  }

}

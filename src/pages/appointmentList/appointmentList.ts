import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Events, MenuController, Platform } from 'ionic-angular';
import { Http } from '@angular/http';
import { AccountServiceProvider } from '../../providers/account-service/account-service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppointmentPage } from '../appointmentCreate/appointmentCreate';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { ToggleGesture } from 'ionic-angular/umd/components/toggle/toggle-gesture';
import { CarCreatePage } from '../carcreate/carcreate';
import { BillPage } from '../bill/bill';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import moment from 'moment';
import { FiltersPage } from '../filters/filters';

@IonicPage()
@Component({
  selector: 'page-repairs',
  templateUrl: 'AppointmentList.html',
})
export class AppointmentListPage {
  cars: any[] = [];
  carList: any[] = [];
  carArray: any[] = [];
  username: string;
  myForm: FormGroup;
  appointmentList: any[] = [];
  appointmentArray: any[] = [];
  appointments: any[] = [];

  pdfObj = null;

  /************FILTER***************/

  state: string = null;
  tipo: string = null;
  filtrado: boolean = false;
  accountId: number;

  constructor(public navCtrl: NavController,
    public http: Http,
    private plt: Platform,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public events: Events,
    public accountServiceProvider: AccountServiceProvider,
    public menuCtrl: MenuController) {

    this.myForm = this.createMyForm();

    this.events.subscribe('state:tipo', (state, tipo) => {
      this.state = state;
      this.tipo = tipo;
    });
  }

  loadUserCredentialsStorage(): string {
    var username = window.localStorage.getItem('user');
    return username;
  }

  ionViewWillEnter() {
    this.presentLoading();
  }

  //Spinner between two screens
  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 300
    });
    //after charge, get the cars and initialize the list
    this.username = this.loadUserCredentialsStorage();
    this.cars = this.carList;
    this.appointments = this.appointmentList;
    this.getCars();
    if (this.appointments != null && this.appointments.length > 1
      && (this.state != null || this.tipo != null)) {
      loader.present().then(() => {
        this.applyFilters(this.state, this.tipo).then(() => {
          loader.dismiss();
        });
      });
    }
  }


  public createMyForm() {
    return this.formBuilder.group({
      estado: ['', Validators.required]
    });
  }

  setState(appointmentId: any) {
    let estado = this.myForm.get('estado').value;

    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 300
    });
    loader.present().then(() => {
      this.accountServiceProvider.updateAppointment(appointmentId, estado)
        .then(data => { });

    });
  }



  deleteAppointment(appointmentId: any) {
    var alert: any;
    this.accountServiceProvider.deleteAppointment(appointmentId)
      .subscribe((data) => {
        if (data) {
          alert = this.alertCtrl.create({
            title: "HECHO",
            subTitle: "Cita eliminada de forma correcta",
            buttons: ['OK']
          });
          this.presentLoading();
          alert.present();
        }
        else {
          alert = this.alertCtrl.create({
            title: 'Error',
            subTitle: "No se ha podido eliminar la cita seleccionada",
            buttons: ['OK']
          });
          alert.present();
        }
      });
  }

  addBill(appointment) {
    this.navCtrl.push(BillPage, { appointment: appointment });
  }

  deleteBill(billId) {
    this.accountServiceProvider.deleteBill(billId)
      .subscribe((data) => {
        if (data) {
          var alert = this.alertCtrl.create({
            title: "Success",
            subTitle: "Bill deleted from Appointment",
            buttons: ['OK']
          });
          this.presentLoading();
          alert.present();
        }
        else {
          var alert = this.alertCtrl.create({
            title: 'Error',
            subTitle: "{{ 'please' | translate }}",
            buttons: ['OK']
          });
          alert.present();
        }
      });;
  }


  initializeCars() {
    this.cars = this.carList;
    this.appointments = this.appointmentList;

  }


  getCars() {
    let carsArray = [];
    let appointmentArray = [];
    this.accountServiceProvider.getCarsService()
      .subscribe((data) => {
        let cars = data.json();
        if (cars.length != 0) {
          for (var i of cars) {
            for (var j of i.appointmentList) {
              appointmentArray.push(j);
            }
            carsArray.push(i);
          }
          this.appointmentList = appointmentArray;
          this.carList = carsArray;
     
            this.initializeCars();

        }
      });
  }


  saveData() {
    this.navCtrl.setRoot(AppointmentPage);
  }


  /************************FILTERS*******************************/

  filters() {
    this.navCtrl.push(FiltersPage);
  }

  applyFilters(state: string, tipo: string) {
    return new Promise((resolve) => {
      this.filtrado = false;
      this.initializeCars();
      this.appointments = this.appointments.filter((item) => {
        if (state != null && tipo != null) {
          this.filtrado = true;
          return (item.state == state && item.tipo == tipo);
        }
        else if (state == null && tipo != null) {
          this.filtrado = true;
          return (item.tipo == this.tipo);
        }
        else if (state != null && tipo == null) {
          this.filtrado = true;
          return (item.state == this.state);
        }
      })
      resolve(true);
    });
  }

  cleanFilters() {
    this.tipo = null;
    this.state = null;
    this.initializeCars();
  }





  createPdf(bill: any) { //recibimos directamente los datos de la factura
    //recordemos que para una cita hay solo una factura OneToOne relationship

    var docDefinition = { //definimos el estilo del pdf y su contenido
      content: [
        { text: 'CarAuto', style: 'header' },
        { text: moment(Date.now()).format('DD-MM-YYYY'), alignment: 'right' },

        { text: 'From', style: 'subheader' },
        "Cochify",
        "Direccion: Calle ",
        "Pais: España",

        { text: 'To', style: 'subheader' },
        "Noelia Cotelo",
        "Direccion: Malpica",
        "Pais: España",

        { text: 'Desglose', style: 'subheader' },
        {
          style: 'itemsTable',
          table: {
            widths: ['*', 75, 75], //hay tres columnas en la tabla, cada una con su ancho
            body: [
              [
                { text: 'Description', style: 'itemsTableHeader' },
                { text: 'Quantity', style: 'itemsTableHeader' },
                { text: 'Price', style: 'itemsTableHeader' },
              ], //cada conjunto de [] es una fila de la tabla
              [
                bill.description, bill.quantity, bill.importe
              ],
              [
                { text: 'Promociones', style: 'itemsTableHeader' },
                { text: 'Quantity', style: 'itemsTableHeader' },
                { text: 'Price', style: 'itemsTableHeader' },
              ], //cada conjunto de [] es una fila de la tabla
              [
                bill.description,bill.quantity, bill.importe
              ]
            ]
          }
        },
        {
          style: 'totalsTable',
          table: {
            widths: ['*', 75, 75], //hay tres columnas de datos, cada una con su ancho
            body: [
              [
                '',
                'Subtotal',
                bill.importe,
              ],
              [
                '',
                'IVA (21%)',
                bill.iva,
              ],
              [
                '',
                'Promociones',
                bill.total,
              ],
              [
                '',
                'Total (€)',
                bill.total,
              ]
            ]
          },
          layout: 'noBorders'
        },
      ],
      styles: {
        header: {
          fontSize: 20,
          bold: true,
          margin: [0, 0, 0, 10],
          alignment: 'right'
        },
        subheader: {
          fontSize: 16,
          bold: true,
          margin: [0, 20, 0, 5]
        },
        itemsTable: {
          margin: [0, 5, 0, 15]
        },
        itemsTableHeader: {
          bold: true,
          fontSize: 13,
          color: 'black'
        },
        totalsTable: {
          bold: true,
          margin: [0, 30, 0, 0]
        }
      },
      defaultStyle: {
      }
    }
    this.pdfObj = pdfMake.createPdf(docDefinition);
  }

  downloadPdf(appointment: any) {
    this.createPdf(appointment.bill) //cuando clicka en descargar, primero general el pdf
    if (this.plt.is('cordova')) {
      //abre el pdf en caso de ser nativo
      this.pdfObj.open();
    } else {
      // On a browser simply use download!
      this.pdfObj.download(); //una vez generado el pdf con el primer metodo (linea 229), ejecutal la descarga.
      //download es un metodo predefinidom de la libreria para pdfs de Ionic
    }
  }
}



import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, Nav } from 'ionic-angular';
import { AlertController } from "ionic-angular";
import { LoadingController } from 'ionic-angular';
import { AccountServiceProvider } from "../../providers/account-service/account-service";
import { CarDetailPage } from '../car-detail/car-detail';
import { CarCreatePage } from '../carcreate/carcreate';

@IonicPage()
@Component({
  selector: 'page-cars',
  templateUrl: 'cars.html'
})
export class ListCarsPage {
  carsArray: any[] = [];
  carsList: any[] = [];
  cars: any[] = [];
  username: string;
  carsStateArray: any[] = [];
  carsStateList: any[] = [];
  carsState: any[] = [];


  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public accountService: AccountServiceProvider,
    public nav: Nav
  ) {
  }

  loadUserCredentialsStorage(): string {
    var username = window.localStorage.getItem('user');
    return username;
  }


  ionViewWillEnter() {
    this.presentLoading();
  }

  presentLoading() {
    let loader = this.loadingCtrl.create({
      content: "Please wait...",
      duration: 30

    });
    //after charge, get the Cities and initialize the list
    this.username = this.loadUserCredentialsStorage();
    this.carsList = this.carsArray;
    this.carsStateList = this.carsStateArray;
    this.getUserCars();
    this.getCarsAdmin();
    loader.present();
  
  }

  initializeCars() {
    this.cars = this.carsList;
  }

  initializeCarsState() {
    this.carsState = this.carsStateList;
  }


  getUserCars() {
    return new Promise((resolve) => {
      let carsArray = [];
      this.accountService.getUserCarsService(this.username)
        .subscribe((data) => {
          let cars = data.json();
          if (cars.length != 0) {
            for (var i of cars) {
              carsArray.push(i);
            }
            this.carsList = carsArray;
            this.initializeCars();
          }
        });
      resolve(true);
    });
  }

  getCarsAdmin() {
    return new Promise((resolve) => {
      let carsStateArray = [];
      this.accountService.getCarsService()
        .subscribe((data) => {
          let cars = data.json();
          if (cars.length != 0) {
            for (var i of cars) {
              for (var j of i.appointmentList) {
                if (j.state == "Pendiente")
                  carsStateArray.push(i);
              }
              this.carsStateList = carsStateArray;
              this.initializeCarsState();
            }
          }
        });
      resolve(true);
    });
  }


  getDetail(car: any) {
    this.navCtrl.push(CarDetailPage, { car: car });
  }

  saveData() {
    this.navCtrl.push(CarCreatePage);
  }

  deleteCar(carId: any) {
    var alert: any;
    this.accountService.deleteCar(carId)
      .subscribe((data) => {
        if (data) {
          alert = this.alertCtrl.create({
            title: "HECHO",
            subTitle: "Coche eliminado de forma correcta",
            buttons: ['OK']
          });
          this.navCtrl.setRoot(ListCarsPage);
          alert.present();
        }
        else {
          alert = this.alertCtrl.create({
            title: 'Error',
            subTitle: "No se ha podido eliminar el coche seleccionado",
            buttons: ['OK']
          });
          alert.present();
        }
      });
  }
}


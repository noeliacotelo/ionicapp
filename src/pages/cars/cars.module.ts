import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListCarsPage } from './cars';

@NgModule({
  declarations: [
    ListCarsPage,
  ],
  imports: [
    IonicPageModule.forChild(ListCarsPage),
  ],
})
export class CarsPageModule {}

import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { Headers, RequestOptions } from '@angular/http';
import { URL_SERVICIOS } from "../../../config/urlService";
import 'rxjs/add/operator/map';


@Injectable()
export class AccountServiceProvider {
  getCarsStateService() {
    throw new Error("Method not implemented.");
  }

  isLoggedin: boolean;

  constructor(public http: Http) {
    this.http = http;
    this.isLoggedin = false;
  }


  destroyUserCredentials() {
    this.isLoggedin = false;
    window.localStorage.clear();
  }

  loginService(email: string, password: string) {
    let account = {
      email: email,
      password: password
    }

    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    let options = new RequestOptions({
      headers: headers
    });

    let url = URL_SERVICIOS + "/login";

    let json = JSON.stringify(account); //the JSON in string format

    return new Promise(resolve => {
      this.http.post(url, json, options).subscribe(data => {
        if (data.json()) {
          resolve(true);
        }
        else {
          resolve(false);
        }
      });
    });

  }

  signupService(name: string, email: string, password: string) {
    let account = {
      name: name,
      email: email,
      password: password
    }

    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    let options = new RequestOptions({
      headers: headers
    });

    let url = URL_SERVICIOS + "/signup";

    let json = JSON.stringify(account); //the JSON in string format

    return new Promise(resolve => {
      this.http.post(url, json, options).subscribe(data => {
        if (data.json()) {
          resolve(true);
        }
        else {
          resolve(false);
        }
      });
    });
  }

  getUser(username: string) {

    let url = URL_SERVICIOS + "/account/" + username;

    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    let options = new RequestOptions({
      headers: headers
    });

    return this.http.get(url, options).map((res: any) => res)

  }

  getListUsers() {
    let url = URL_SERVICIOS + "/users";

    let headers = new Headers({
      'Content-Type': 'application/json'
    });


    let options = new RequestOptions({
      headers: headers
    });

    return this.http.get(url, options).map((res: any) => res)
  }

  getCarsService() {

    let url = URL_SERVICIOS + "/car";

    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    let options = new RequestOptions({
      headers: headers
    });

    return this.http.get(url, options).map((res: any) => res)

  }


  setCars( email:string,name: string, matricula: string,km: number,ano: number, combustible: string, color: string, potencia: number) {
    let car = {
      email:email,
      name: name,
      matricula: matricula,
      km:km,
      ano: ano,
      combustible: combustible,
      color: color,
      potencia: potencia
    }


    let url = URL_SERVICIOS + "/car/" + email;

    let json = JSON.stringify(car); //the JSON in string format

    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    let options = new RequestOptions({
      headers: headers
    });

    return new Promise(resolve => {
      this.http.post(url, json, options).subscribe(data => {
        if (data.json()) {
          resolve(true);
        }
        else {
          resolve(false);
        }
      });
    });
  }




  getAppointmentService() {

    let url = URL_SERVICIOS + "/appointment";

    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    let options = new RequestOptions({
      headers: headers
    });

    return this.http.get(url, options).map((res: any) => res)

  }

  setAppointment(hours: string, date: string, tipo: string, car: string,promo:number, email: string) {
    let descripcion:string="desc";
    let state:string="Pendiente";
    let cita = {
      date: date,
      descripcio:descripcion,
      hours: hours,
      state:state,
      tipo: tipo,
      promo:promo
    }

    let url = URL_SERVICIOS + "/appointmentCreate/" + car + "/" + email;

    let json = JSON.stringify(cita); //the JSON in string format

    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    let options = new RequestOptions({
      headers: headers
    });

    return new Promise(resolve => {
      this.http.post(url, json, options).subscribe(data => {
        if (data.json()) {
          resolve(true);
        }
        else {
          resolve(false);
        }
      });
    });
  }

  setBill(descripcion: string, importe: any,cantidad: any, appointmentId: any, date:any,cif:any) {
    let bill = {
      description: descripcion,
      fecha:date,
      quantity: cantidad,
      importe: importe,
      cif:cif
    }

    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    let options = new RequestOptions({
      headers: headers
    });

    let url = URL_SERVICIOS + "/bill/" + appointmentId;

    let json = JSON.stringify(bill); //the JSON in string format

    return new Promise(resolve => {
      this.http.post(url, json, options).subscribe(data => {
        if (data.json()) {
          resolve(true);
        }
        else {
          resolve(false);
        }
      });
    });
  }


  updateAppointment(appointmentId: string, state: any) {

    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    let options = new RequestOptions({
      headers: headers
    });

    let url = URL_SERVICIOS + "/update/" + appointmentId + "/" + state;

    return new Promise(resolve => {
      this.http.post(url, options).subscribe(data => {
        if (data.json()) {
          resolve(true);
        }
        else {
          resolve(false);
        }
      });
    });
  }


  getUserCarsService(email: string) {
    let url = URL_SERVICIOS + "/carsAccount/" + email;

    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    let options = new RequestOptions({
      headers: headers
    });

    return this.http.get(url, options).map((res: any) => res)
  }

  getAppointmentUser(email: string) {
    let url = URL_SERVICIOS + "/appointment/" + email;

    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    let options = new RequestOptions({
      headers: headers
    });

    return this.http.get(url, options).map((res: any) => res)
  }

  getPromosService() {
    let url = URL_SERVICIOS + "/appointment";

    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    let options = new RequestOptions({
      headers: headers
    });

    return this.http.get(url, options).map((res: any) => res)
  }


  deleteCar(carId: any) {

    let url = URL_SERVICIOS + "/deleteCar/" + carId;

    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    let options = new RequestOptions({
      headers: headers
    });

    return this.http.delete(url, options).map((res: any) => res)
  }

  deleteUser(email: string) {
    
    let url = URL_SERVICIOS + "/signoff/" + email;

    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    let options = new RequestOptions({
      headers: headers
    });

    return this.http.delete(url, options).map((res: any) => res)
  }

  deleteAppointment(appointmentId: any) {

    let url = URL_SERVICIOS + "/deleteAppointment/" + appointmentId;

    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    let options = new RequestOptions({
      headers: headers
    });

    return this.http.delete(url, options).map((res: any) => res)
  }

  deleteBill(billId: any) {

    let url = URL_SERVICIOS + "/bill/" + billId;

    let headers = new Headers({
      'Content-Type': 'application/json'
    });

    let options = new RequestOptions({
      headers: headers
    });

    return this.http.delete(url, options).map((res: any) => res)
  }




}
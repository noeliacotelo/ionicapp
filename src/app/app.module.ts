import { NgModule, ErrorHandler} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { TabsPage } from '../pages/tabs/tabs';
import { ListCarsPage } from '../pages/cars/cars';
import { LanguagePage } from '../pages/language/language';

import { EmailComposer } from '@ionic-native/email-composer';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { WelcomePage } from '../pages/welcome/welcome';
import { LoginPage } from '../pages/login/login';
import { AccountServiceProvider } from '../providers/account-service/account-service';
import { SignupPage } from '../pages/signup/signup';
import { CarDetailPage } from '../pages/car-detail/car-detail';
import { CarCreatePage } from '../pages/carcreate/carcreate';
import { ProfilePage } from '../pages/profile/profile';
import { AppointmentPage } from '../pages/appointmentCreate/appointmentCreate';
import {AppointmentListPage} from '../pages/appointmentList/appointmentList';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { BillPage } from '../pages/bill/bill';
import { ForgotPasswordPage } from '../pages/forgot-password/forgot-password';
import { FiltersPage } from '../pages/filters/filters';
import {Push} from '@ionic-native/push/ngx'


export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    TabsPage,
    WelcomePage,
    LoginPage,
    SignupPage,
    ListCarsPage,
    CarDetailPage,
    CarCreatePage,
    ProfilePage,
    AppointmentPage,
    AppointmentListPage,
    LanguagePage,
    ContactPage,
    BillPage,
    ForgotPasswordPage,
    FiltersPage

  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    TabsPage,
    WelcomePage,
    LoginPage,
    SignupPage,
    ListCarsPage,
    CarDetailPage,
    CarCreatePage,
    ProfilePage,
    AppointmentPage,
    AppointmentListPage,
    LanguagePage,
    BillPage,
    ForgotPasswordPage,
    FiltersPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Push,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AccountServiceProvider,
    File, //añadido para generar pdfs
    FileOpener, //añadido para generar pdfs
    EmailComposer //añadido para generar el email
  ]
})
export class AppModule {}

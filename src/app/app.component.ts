import { Component, ViewChild } from '@angular/core';
import { Nav,Platform, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MenuController } from 'ionic-angular';
import { AccountServiceProvider } from '../providers/account-service/account-service';
import {LoginPage} from '../pages/login/login';
import { ListCarsPage } from '../pages/cars/cars';
import { TabsPage } from '../pages/tabs/tabs';
import { ProfilePage } from '../pages/profile/profile';
import { LanguagePage } from '../pages/language/language';
import { TranslateService } from '@ngx-translate/core';
import { ContactPage } from '../pages/contact/contact';
import { AppointmentListPage } from '../pages/appointmentList/appointmentList';
import { AppointmentPage } from '../pages/appointmentCreate/appointmentCreate';
import { Push, PushObject, PushOptions } from '@ionic-native/push/ngx';



@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = LoginPage;
  pages: Array<{title:string, component: any}>;
  @ViewChild(Nav) nav: Nav;

  constructor(platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    public menuCtrl: MenuController,
    public loadingCtrl:LoadingController,
    public accountService : AccountServiceProvider,
    private translate: TranslateService,
    private push:Push){

    let language = translate.getBrowserLang();
    this.translate.setDefaultLang(language);
    this.translate.use(language); 

    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();

      this.pushSetup();
    });
    
    this.pages=[
      {title:'Coche', component: ListCarsPage},
      {title:'Profile', component: ProfilePage},
      {title:'Citas', component: AppointmentListPage},
      {title:'Crear cita', component: AppointmentPage}
    ]

}
pushSetup(){
  const options: PushOptions = {
     android: {
         // Añadimos el sender ID para Android.
         senderID: '560790819032'
     },
     ios: {
         alert: 'true',
         badge: true,
         sound: 'false'
     }
  };

  const pushObject: PushObject = this.push.init(options);

  pushObject.on('notification').subscribe((notification: any) => console.log('Received a notification', notification));
  pushObject.on('registration').subscribe((registration: any) => console.log('Device registered', registration));
  pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));

  }

  Coches(){
    this.menuCtrl.close();
    this.nav.setRoot(TabsPage);
  }

  logout(){
    this.menuCtrl.close();
    this.nav.setRoot(LoginPage);
  }

  languages() {
    this.menuCtrl.close();
    this.nav.setRoot(LanguagePage);
  }

  contact() {
    this.menuCtrl.close();
    this.nav.setRoot(ContactPage);
  }

  Profile(){
    this.menuCtrl.close();
    this.nav.setRoot(ProfilePage);
  }
}

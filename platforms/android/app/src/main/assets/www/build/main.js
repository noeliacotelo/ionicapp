webpackJsonp([12],{

/***/ 168:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BillPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_account_service_account_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__appointmentList_appointmentList__ = __webpack_require__(70);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the BillPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var BillPage = /** @class */ (function () {
    function BillPage(navCtrl, navParams, viewCtrl, formBuilder, alertCtrl, loadingCtrl, accountService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.formBuilder = formBuilder;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.accountService = accountService;
        this.myForm = this.createMyForm();
        this.appointment = navParams.get('appointment');
    }
    BillPage.prototype.ionViewDidLoad = function () {
        this.presentLoading();
    };
    BillPage.prototype.createMyForm = function () {
        return this.formBuilder.group({
            descripcion: [''],
            importe: [''],
            iva: [''],
            total: [''],
            cantidad: ['']
        });
    };
    //Spinner between two screens
    BillPage.prototype.presentLoading = function () {
        var loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 30
        });
        //after charge, get the cars and initialize the list
        loader.dismiss();
    };
    BillPage.prototype.saveData = function (appointmentId, date) {
        var _this = this;
        var descripcion = this.myForm.get('descripcion').value;
        var importe = this.myForm.get('importe').value;
        var cantidad = this.myForm.get('cantidad').value;
        var cif = "48112863Z";
        var loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 30
        });
        loader.present().then(function () {
            _this.accountService.setBill(descripcion, importe, cantidad, appointmentId, date, cif)
                .then(function (data) {
                if (data) {
                    var alert = _this.alertCtrl.create({
                        title: "HECHO",
                        subTitle: "Factura creada correctamente",
                        buttons: ['OK']
                    });
                    alert.present();
                }
            });
            _this.myForm.reset();
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__appointmentList_appointmentList__["a" /* AppointmentListPage */]);
        });
    };
    BillPage.prototype.cancel = function () {
        this.viewCtrl.dismiss();
    };
    BillPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-bill',template:/*ion-inline-start:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\bill\bill.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>{{"CrearFactura" | translate}}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <form [formGroup]="myForm">\n\n    <ion-list>\n\n      <ion-item>\n\n        <ion-input type="text" placeholder="Descripcion" [value]=\'descripcion\' formControlName="descripcion">\n\n        </ion-input>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-input type="text" placeholder="Cantidad" [value]="cantidad" formControlName="cantidad"></ion-input>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-input type="text" placeholder="Importe" [value]=\'importe\' formControlName="importe"></ion-input>\n\n      </ion-item>\n\n    </ion-list>\n\n  </form>\n\n\n\n  <ion-buttons center>\n\n    &nbsp;&nbsp;&nbsp;&nbsp;\n\n    <button ion-button icon-left (click)="saveData(appointment.appointmentId, appointment.date)">\n\n      <ion-icon name="md-checkmark">&nbsp;Aceptar</ion-icon>\n\n    </button>\n\n    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n\n    <button ion-button icon-left (click)="cancel()">\n\n      <ion-icon name="md-close">&nbsp;Cancelar</ion-icon>\n\n    </button>\n\n  </ion-buttons>\n\n</ion-content>'/*ion-inline-end:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\bill\bill.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_account_service_account_service__["a" /* AccountServiceProvider */]])
    ], BillPage);
    return BillPage;
}());

//# sourceMappingURL=bill.js.map

/***/ }),

/***/ 169:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FiltersPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_account_service_account_service__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the FiltersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FiltersPage = /** @class */ (function () {
    function FiltersPage(navCtrl, http, navParams, accountService, alertCtrl, loadingCtrl, events) {
        this.navCtrl = navCtrl;
        this.http = http;
        this.navParams = navParams;
        this.accountService = accountService;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.events = events;
        this.states = ["Pendiente", "En curso", "Finalizado", "Cancelado"];
        this.tipos = ["Mantenimiento", "Reparacion"];
    }
    FiltersPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad FiltersPage');
    };
    FiltersPage.prototype.setState = function (state) {
        this.state = state;
    };
    FiltersPage.prototype.setTipo = function (tipo) {
        this.tipo = tipo;
    };
    FiltersPage.prototype.apply = function () {
        this.events.publish('state:tipo', this.state, this.tipo);
        this.navCtrl.pop();
    };
    FiltersPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-filters',template:/*ion-inline-start:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\filters\filters.html"*/'<ion-header>\n\n  <ion-navbar color="primary">\n\n    <ion-title>{{"filters" | translate }}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-header>\n\n  <ion-navbar color="primary">\n\n    <ion-title>{{"filters" | translate }}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-list no-lines>\n\n    <ion-row>\n\n      <ion-col col-12>\n\n        <ion-item class="selected">\n\n          <ion-label fixed color="primary"> {{"state" | translate }} </ion-label>\n\n          <ion-select [(ngModel)]="state" (ionChange)="setState(state)">\n\n            <div class="input_area">\n\n              <ion-option *ngFor="let state of states" [value]="state">\n\n                {{state}}\n\n              </ion-option>\n\n            </div>\n\n          </ion-select>\n\n        </ion-item>\n\n      </ion-col>\n\n      <ion-col col-12>\n\n        <ion-item class="selected">\n\n          <ion-label fixed color="primary"> {{"tipos" | translate }} </ion-label>\n\n          <ion-select [(ngModel)]="tipo" (ionChange)="setTipo(tipo)">\n\n            <div class="input_area">\n\n              <ion-option *ngFor="let tipo of tipos" [value]="tipo">\n\n                {{tipo}}\n\n              </ion-option>\n\n            </div>\n\n          </ion-select>\n\n        </ion-item>\n\n      </ion-col>\n\n    </ion-row>\n\n    <div padding>\n\n      <button ion-button block (click)="apply()">{{"filter" | translate }}</button>\n\n    </div>\n\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\filters\filters.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__providers_account_service_account_service__["a" /* AccountServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */]])
    ], FiltersPage);
    return FiltersPage;
}());

//# sourceMappingURL=filters.js.map

/***/ }),

/***/ 170:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CarDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_account_service_account_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__cars_cars__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CarDetailPage = /** @class */ (function () {
    function CarDetailPage(navCtrl, http, navParams, accountServiceProvider, alertCtrl, loadingCtrl, formBuilder) {
        this.navCtrl = navCtrl;
        this.http = http;
        this.navParams = navParams;
        this.accountServiceProvider = accountServiceProvider;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.formBuilder = formBuilder;
        this.car = navParams.get('car'); //Get the car from the previous page
    }
    CarDetailPage.prototype.ionViewWillEnter = function () {
        this.presentLoading();
    };
    CarDetailPage.prototype.loadUserCredentialsStorage = function () {
        var username = window.localStorage.getItem('user');
        return username;
    };
    //Spinner between two screens
    CarDetailPage.prototype.presentLoading = function () {
        var loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 300
        });
        //after charge, get the cars and initialize the list
        this.username = this.loadUserCredentialsStorage();
        loader.present();
    };
    CarDetailPage.prototype.deleteCar = function (carId) {
        var _this = this;
        var alert;
        this.accountServiceProvider.deleteCar(carId)
            .subscribe(function (data) {
            if (data) {
                alert = _this.alertCtrl.create({
                    title: "HECHO",
                    subTitle: "Coche eliminado de forma correcta",
                    buttons: ['OK']
                });
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__cars_cars__["a" /* ListCarsPage */]);
                alert.present();
            }
            else {
                alert = _this.alertCtrl.create({
                    title: 'Error',
                    subTitle: "No se ha podido eliminar el coche seleccionado",
                    buttons: ['OK']
                });
                alert.present();
            }
        });
    };
    CarDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-car-detail',template:/*ion-inline-start:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\car-detail\car-detail.html"*/'<ion-header>\n\n  <ion-navbar color="primary">\n\n    <ion-title>{{"Detalles" | translate}}</ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-list no-lines class="lista">\n\n    <ion-item>\n\n      <small>{{"Marca" | translate}}</small><br>\n\n      <p>{{car.name}}</p>\n\n    </ion-item>\n\n    <ion-item>\n\n      <small>{{"Coste" | translate}}</small><br>\n\n      <p>{{car.km}} KM</p>\n\n    </ion-item>\n\n    <ion-item>\n\n      <small>{{"Anio" | translate}}</small><br>\n\n      <p>{{car.ano}}</p>\n\n    </ion-item>\n\n    <ion-item>\n\n      <small>{{"Combustible" | translate}}</small><br>\n\n      <p>{{car.combustible}}</p>\n\n    </ion-item>\n\n    <ion-item>\n\n      <small>{{"Color" | translate}}</small><br>\n\n      <p>{{car.color}}</p>\n\n    </ion-item>\n\n    <ion-item>\n\n      <small>{{"Potencia" | translate}}</small><br>\n\n      <p>{{car.potencia}} cv</p>\n\n    </ion-item>\n\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\car-detail\car-detail.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__providers_account_service_account_service__["a" /* AccountServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */]])
    ], CarDetailPage);
    return CarDetailPage;
}());

//# sourceMappingURL=car-detail.js.map

/***/ }),

/***/ 171:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CarCreatePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_account_service_account_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__cars_cars__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CarCreatePage = /** @class */ (function () {
    function CarCreatePage(navCtrl, navParams, viewCtrl, formBuilder, alertCtrl, loadingCtrl, accountService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.formBuilder = formBuilder;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.accountService = accountService;
        this.myForm = this.createMyForm();
        // Watch the form for changes, and
    }
    CarCreatePage.prototype.loadUserCredentialsStorage = function () {
        var username = window.localStorage.getItem('user');
        return username;
    };
    CarCreatePage.prototype.ionViewWillEnter = function () {
        this.presentLoading();
    };
    CarCreatePage.prototype.createMyForm = function () {
        return this.formBuilder.group({
            marca: [''],
            matricula: new __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* FormControl */]('', [__WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].minLength(7), __WEBPACK_IMPORTED_MODULE_1__angular_forms__["g" /* Validators */].maxLength(7)]),
            color: [''],
            km: [''],
            potencia: [''],
            ano: [''],
            combustible: ['']
        });
    };
    //Spinner between two screens
    CarCreatePage.prototype.presentLoading = function () {
        var loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 30
        });
        //after charge, get the cars and initialize the list
        this.username = this.loadUserCredentialsStorage();
        loader.present();
    };
    CarCreatePage.prototype.saveData = function () {
        var _this = this;
        var name = this.myForm.get('marca').value;
        var matricula = this.myForm.get('matricula').value;
        var km = this.myForm.get('km').value;
        var ano = this.myForm.get('ano').value;
        var combustible = this.myForm.get('combustible').value;
        var color = (this.myForm.get('color').value).toString();
        var potencia = this.myForm.get('potencia').value;
        var loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 30
        });
        loader.present().then(function () {
            _this.accountService.setCars(_this.username, name, matricula, km, ano, combustible, color, potencia)
                .then(function (data) {
                if (data) {
                    var alert = _this.alertCtrl.create({
                        title: "Success",
                        subTitle: "Car created correctly",
                        buttons: ['OK']
                    });
                    alert.present();
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__cars_cars__["a" /* ListCarsPage */]);
                }
                else {
                    var alert = _this.alertCtrl.create({
                        title: "Success",
                        subTitle: "Car created correctly",
                        buttons: ['OK']
                    });
                    alert.present();
                }
            });
        });
    };
    CarCreatePage.prototype.cancel = function () {
        this.viewCtrl.dismiss();
    };
    CarCreatePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-car-create',template:/*ion-inline-start:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\carcreate\carcreate.html"*/'<ion-header>\n\n  <ion-toolbar color="primary">\n\n    <ion-title>{{"CrearCoche" | translate}}</ion-title>\n\n    <ion-buttons right>\n\n      <button ion-button (click)="cancel()">\n\n        <ion-icon name="md-close"></ion-icon>\n\n      </button>\n\n      <button ion-button (click)="saveData()" [disabled]="marca == null || color == null || matricula == null || km == null || potencia == null || ano == null || combustible == null">\n\n        <ion-icon name="md-checkmark"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n  </ion-toolbar>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <form [formGroup]="myForm">\n\n    <ion-list class="lista" >\n\n      <ion-item class="items">\n\n        <ion-input placeholder="Marca" [(ngModel)]="marca" type="text" required="true" clearInput=true formControlName="marca" class="dos"></ion-input>\n\n      </ion-item>\n\n      <ion-item class="items">\n\n        <small>{{"Matricula" | translate}}</small><br>\n\n        <ion-input placeholder="Matricula" [(ngModel)]="matricula" type="text" required="true" clearInput=true class="dos" formControlName="matricula"></ion-input>\n\n      </ion-item>\n\n      <div class="error" no-lines color="none" *ngIf="( myForm.get(\'matricula\').hasError(\'minlength\') || myForm.get(\'matricula\').hasError(\'maxlength\'))">\n\n        <div *ngIf="myForm.get(\'matricula\').touched && myForm.get(\'matricula\').hasError(\'minlength\') ">\n\n          Mínimo 4 números y 3 caracteres \n\n        </div>\n\n      </div>\n\n      <ion-item class="items">\n\n        <ion-input type="text" [(ngModel)]="color"placeholder="Color" formControlName="color" class="dos"></ion-input>\n\n      </ion-item>\n\n      <ion-item class="items">\n\n        <ion-input type="number" [(ngModel)]="km" placeholder="Kilometraje" required="true" clearInput=true min="0" max="900000"\n\n          formControlName="km" class="dos"></ion-input>\n\n      </ion-item>\n\n      <ion-item class="items">\n\n        <ion-input type="number" [(ngModel)]="potencia" placeholder="Potencia (cv)" required="true" clearInput=true min="0" max="600"\n\n          formControlName="potencia" class="dos"></ion-input>\n\n      </ion-item>\n\n      <ion-item class="items" >\n\n        <ion-datetime placeholder="Año de fabricación" [(ngModel)]="ano" required="true" clearInput=true displayFormat="YYYY" formControlName="ano"> </ion-datetime>\n\n      </ion-item>\n\n      <ion-item class="items">\n\n        <ion-select  [(ngModel)]="combustible" formControlName="combustible" placeholder="Combustible">\n\n          <ion-option>Gasoil</ion-option>\n\n          <ion-option>Gasolina</ion-option>\n\n          <ion-option>Eléctrico</ion-option>\n\n          <ion-option>Híbrido</ion-option>\n\n        </ion-select>\n\n      </ion-item>\n\n    </ion-list>\n\n  </form>\n\n</ion-content>'/*ion-inline-end:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\carcreate\carcreate.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["m" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_account_service_account_service__["a" /* AccountServiceProvider */]])
    ], CarCreatePage);
    return CarCreatePage;
}());

//# sourceMappingURL=carcreate.js.map

/***/ }),

/***/ 172:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_email_composer__ = __webpack_require__(362);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ContactPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ContactPage = /** @class */ (function () {
    function ContactPage(navCtrl, emailComposer) {
        this.navCtrl = navCtrl;
        this.emailComposer = emailComposer;
    }
    ContactPage.prototype.send = function () {
        this.emailComposer.isAvailable().then(function (available) {
            if (available) {
                console.log('posible');
            }
            else {
                console.log('imposible');
            }
        });
        var emailSupport = {
            to: 'talleresantonio@gmail.com',
            cc: [],
            bcc: [],
            attachment: [],
            subject: this.subject,
            body: this.body,
            isHtml: false
        };
        var emailRecibido = {
            to: this.to,
            cc: [],
            bcc: [],
            attachment: [],
            subject: 'Ha contactado con support',
            body: ' En breve nos pondremos en contacto con usted',
            isHtml: false
        };
        this.emailComposer.open(emailSupport);
        this.emailComposer.open(emailRecibido);
    };
    ContactPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ContactPage');
    };
    ContactPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-contact',template:/*ion-inline-start:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\contact\contact.html"*/'<ion-header>\n\n  <ion-toolbar color="primary">\n\n    <ion-buttons left menuToggle>\n\n      <button ion-button icon-only>\n\n        <ion-icon name="menu"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n    <ion-title>{{"contact" | translate }}</ion-title>\n\n  </ion-toolbar>\n\n</ion-header>\n\n\n\n<ion-content scroll="false">\n\n    <ion-list no-lines>\n\n      <ion-item>\n\n        <ion-icon name="ios-person-outline" item-start></ion-icon>\n\n        <ion-label stacked>{{"name" | translate}}</ion-label>\n\n        <ion-input type="text" placeholder="{{\'completeName\' | translate}}"></ion-input>\n\n      </ion-item>\n\n      <ion-item>\n\n        <ion-icon name="ios-mail-outline" item-start></ion-icon>\n\n        <ion-label stacked>{{"mail" | translate}}</ion-label>\n\n        <ion-input type="email" [(ngModel)] = \'to\' placeholder="Email"></ion-input>\n\n      </ion-item>\n\n      <ion-item>\n\n          <ion-textarea no-lines [(ngModel)] = \'body\' placeholder="{{\'input\' | translate}}"></ion-textarea>\n\n      </ion-item>\n\n    </ion-list>\n\n    <div padding>\n\n      <button ion-button block type="submit" (click)="send()">{{"enviar" | translate}}</button>\n\n    </div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\contact\contact.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_email_composer__["a" /* EmailComposer */]])
    ], ContactPage);
    return ContactPage;
}());

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 173:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForgotPasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login_login__ = __webpack_require__(82);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the ForgotPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ForgotPasswordPage = /** @class */ (function () {
    function ForgotPasswordPage(navCtrl, navParams, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
    }
    ForgotPasswordPage.prototype.ionViewDidLoad = function () {
    };
    ForgotPasswordPage.prototype.resetPassword = function () {
        var alert = this.alertCtrl.create({
            title: 'Solicitud recibida',
            subTitle: "En breve recibirá un correo con la información para su acceso",
            buttons: ['OK']
        });
        alert.present();
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__login_login__["a" /* LoginPage */]);
    };
    ForgotPasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-forgot-password',template:/*ion-inline-start:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\forgot-password\forgot-password.html"*/'<ion-header>\n\n\n\n  <ion-navbar color=primary>\n\n    <ion-title> Recuperación de contraseña</ion-title>\n\n  </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n  <div class="forgotpassword">\n\n    <div class="image_section">\n\n      <img src="assets/img/forgotpassword.svg" alt="">\n\n    </div>\n\n    <div class="input_section">\n\n      <h3>¿Has olvidado tu contraseña?</h3>\n\n      <p>Introduce el email para recuperarla</p>\n\n      <div class="input_area">\n\n        <ion-icon name="md-mail"></ion-icon>\n\n        <input type="text">\n\n      </div>\n\n      <button ion-button full color=primary margin-top (tap)="resetPassword()">\n\n        Recuperar contraseña\n\n    </button>\n\n    </div>\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\forgot-password\forgot-password.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */]])
    ], ForgotPasswordPage);
    return ForgotPasswordPage;
}());

//# sourceMappingURL=forgot-password.js.map

/***/ }),

/***/ 174:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LanguagePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__tabs_tabs__ = __webpack_require__(48);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LanguagePage = /** @class */ (function () {
    function LanguagePage(navCtrl, translateService) {
        this.navCtrl = navCtrl;
        this.translateService = translateService;
        this.languages = [];
        this.languages = [
            {
                value: 'es',
                label: 'Español',
                img: 'assets/icon/spain.ico'
            },
            {
                value: 'en',
                label: 'Inglés',
                img: 'assets/icon/uk.ico'
            }
        ];
    }
    LanguagePage.prototype.choose = function (lang) {
        this.translateService.use(lang);
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__tabs_tabs__["a" /* TabsPage */]);
    };
    LanguagePage.prototype.ionViewDidLoad = function () {
    };
    LanguagePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-language',template:/*ion-inline-start:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\language\language.html"*/'<ion-header>\n\n  <ion-toolbar color="primary">\n\n    <ion-buttons left menuToggle>\n\n      <button ion-button icon-only>\n\n        <ion-icon name="menu"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n    <ion-title>{{"languages" | translate }}</ion-title>\n\n  </ion-toolbar>\n\n</ion-header>\n\n\n\n<ion-content>\n\n  <ion-list no-lines>\n\n    <ion-item  *ngFor="let language of languages" (tap)="choose(language.value)">\n\n      <ion-thumbnail slot="start">\n\n        <img src={{language.img}}> {{language.label}}\n\n      </ion-thumbnail>\n\n    </ion-item>\n\n  </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\language\language.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__ngx_translate_core__["c" /* TranslateService */]])
    ], LanguagePage);
    return LanguagePage;
}());

//# sourceMappingURL=language.js.map

/***/ }),

/***/ 185:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 185;

/***/ }),

/***/ 229:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/appointmentCreate/appointmentCreate.module": [
		843,
		11
	],
	"../pages/appointmentList/appointmentList.module": [
		844,
		10
	],
	"../pages/bill/bill.module": [
		845,
		9
	],
	"../pages/car-detail/car-detail.module": [
		846,
		8
	],
	"../pages/carcreate/carcreate.module": [
		847,
		7
	],
	"../pages/cars/cars.module": [
		848,
		6
	],
	"../pages/contact/contact.module": [
		849,
		5
	],
	"../pages/filters/filters.module": [
		850,
		4
	],
	"../pages/forgot-password/forgot-password.module": [
		851,
		3
	],
	"../pages/language/language.module": [
		852,
		2
	],
	"../pages/profile/profile.module": [
		853,
		1
	],
	"../pages/welcome/welcome.module": [
		854,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 229;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 24:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccountServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__config_urlService__ = __webpack_require__(536);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AccountServiceProvider = /** @class */ (function () {
    function AccountServiceProvider(http) {
        this.http = http;
        this.http = http;
        this.isLoggedin = false;
    }
    AccountServiceProvider.prototype.getCarsStateService = function () {
        throw new Error("Method not implemented.");
    };
    AccountServiceProvider.prototype.destroyUserCredentials = function () {
        this.isLoggedin = false;
        window.localStorage.clear();
    };
    AccountServiceProvider.prototype.loginService = function (email, password) {
        var _this = this;
        var account = {
            email: email,
            password: password
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        var url = __WEBPACK_IMPORTED_MODULE_2__config_urlService__["a" /* URL_SERVICIOS */] + "/login";
        var json = JSON.stringify(account); //the JSON in string format
        return new Promise(function (resolve) {
            _this.http.post(url, json, options).subscribe(function (data) {
                if (data.json()) {
                    resolve(true);
                }
                else {
                    resolve(false);
                }
            });
        });
    };
    AccountServiceProvider.prototype.signupService = function (name, email, password) {
        var _this = this;
        var account = {
            name: name,
            email: email,
            password: password
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        var url = __WEBPACK_IMPORTED_MODULE_2__config_urlService__["a" /* URL_SERVICIOS */] + "/signup";
        var json = JSON.stringify(account); //the JSON in string format
        return new Promise(function (resolve) {
            _this.http.post(url, json, options).subscribe(function (data) {
                if (data.json()) {
                    resolve(true);
                }
                else {
                    resolve(false);
                }
            });
        });
    };
    AccountServiceProvider.prototype.getUser = function (username) {
        var url = __WEBPACK_IMPORTED_MODULE_2__config_urlService__["a" /* URL_SERVICIOS */] + "/account/" + username;
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.get(url, options).map(function (res) { return res; });
    };
    AccountServiceProvider.prototype.getListUsers = function () {
        var url = __WEBPACK_IMPORTED_MODULE_2__config_urlService__["a" /* URL_SERVICIOS */] + "/users";
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.get(url, options).map(function (res) { return res; });
    };
    AccountServiceProvider.prototype.getCarsService = function () {
        var url = __WEBPACK_IMPORTED_MODULE_2__config_urlService__["a" /* URL_SERVICIOS */] + "/car";
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.get(url, options).map(function (res) { return res; });
    };
    AccountServiceProvider.prototype.setCars = function (email, name, matricula, km, ano, combustible, color, potencia) {
        var _this = this;
        var car = {
            email: email,
            name: name,
            matricula: matricula,
            km: km,
            ano: ano,
            combustible: combustible,
            color: color,
            potencia: potencia
        };
        var url = __WEBPACK_IMPORTED_MODULE_2__config_urlService__["a" /* URL_SERVICIOS */] + "/car/" + email;
        var json = JSON.stringify(car); //the JSON in string format
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return new Promise(function (resolve) {
            _this.http.post(url, json, options).subscribe(function (data) {
                if (data.json()) {
                    resolve(true);
                }
                else {
                    resolve(false);
                }
            });
        });
    };
    AccountServiceProvider.prototype.getAppointmentService = function () {
        var url = __WEBPACK_IMPORTED_MODULE_2__config_urlService__["a" /* URL_SERVICIOS */] + "/appointment";
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.get(url, options).map(function (res) { return res; });
    };
    AccountServiceProvider.prototype.setAppointment = function (hours, date, tipo, car, promo, email) {
        var _this = this;
        var descripcion = "desc";
        var state = "Pendiente";
        var cita = {
            date: date,
            descripcio: descripcion,
            hours: hours,
            state: state,
            tipo: tipo,
            promo: promo
        };
        var url = __WEBPACK_IMPORTED_MODULE_2__config_urlService__["a" /* URL_SERVICIOS */] + "/appointmentCreate/" + car + "/" + email;
        var json = JSON.stringify(cita); //the JSON in string format
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return new Promise(function (resolve) {
            _this.http.post(url, json, options).subscribe(function (data) {
                if (data.json()) {
                    resolve(true);
                }
                else {
                    resolve(false);
                }
            });
        });
    };
    AccountServiceProvider.prototype.setBill = function (descripcion, importe, cantidad, appointmentId, date, cif) {
        var _this = this;
        var bill = {
            description: descripcion,
            fecha: date,
            quantity: cantidad,
            importe: importe,
            cif: cif
        };
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        var url = __WEBPACK_IMPORTED_MODULE_2__config_urlService__["a" /* URL_SERVICIOS */] + "/bill/" + appointmentId;
        var json = JSON.stringify(bill); //the JSON in string format
        return new Promise(function (resolve) {
            _this.http.post(url, json, options).subscribe(function (data) {
                if (data.json()) {
                    resolve(true);
                }
                else {
                    resolve(false);
                }
            });
        });
    };
    AccountServiceProvider.prototype.updateAppointment = function (appointmentId, state) {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        var url = __WEBPACK_IMPORTED_MODULE_2__config_urlService__["a" /* URL_SERVICIOS */] + "/update/" + appointmentId + "/" + state;
        return new Promise(function (resolve) {
            _this.http.post(url, options).subscribe(function (data) {
                if (data.json()) {
                    resolve(true);
                }
                else {
                    resolve(false);
                }
            });
        });
    };
    AccountServiceProvider.prototype.getUserCarsService = function (email) {
        var url = __WEBPACK_IMPORTED_MODULE_2__config_urlService__["a" /* URL_SERVICIOS */] + "/carsAccount/" + email;
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.get(url, options).map(function (res) { return res; });
    };
    AccountServiceProvider.prototype.getAppointmentUser = function (email) {
        var url = __WEBPACK_IMPORTED_MODULE_2__config_urlService__["a" /* URL_SERVICIOS */] + "/appointment/" + email;
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.get(url, options).map(function (res) { return res; });
    };
    AccountServiceProvider.prototype.getPromosService = function () {
        var url = __WEBPACK_IMPORTED_MODULE_2__config_urlService__["a" /* URL_SERVICIOS */] + "/appointment";
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.get(url, options).map(function (res) { return res; });
    };
    AccountServiceProvider.prototype.deleteCar = function (carId) {
        var url = __WEBPACK_IMPORTED_MODULE_2__config_urlService__["a" /* URL_SERVICIOS */] + "/deleteCar/" + carId;
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.delete(url, options).map(function (res) { return res; });
    };
    AccountServiceProvider.prototype.deleteUser = function (email) {
        var url = __WEBPACK_IMPORTED_MODULE_2__config_urlService__["a" /* URL_SERVICIOS */] + "/signoff/" + email;
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.delete(url, options).map(function (res) { return res; });
    };
    AccountServiceProvider.prototype.deleteAppointment = function (appointmentId) {
        var url = __WEBPACK_IMPORTED_MODULE_2__config_urlService__["a" /* URL_SERVICIOS */] + "/deleteAppointment/" + appointmentId;
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.delete(url, options).map(function (res) { return res; });
    };
    AccountServiceProvider.prototype.deleteBill = function (billId) {
        var url = __WEBPACK_IMPORTED_MODULE_2__config_urlService__["a" /* URL_SERVICIOS */] + "/bill/" + billId;
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]({
            headers: headers
        });
        return this.http.delete(url, options).map(function (res) { return res; });
    };
    AccountServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_http__["b" /* Http */]])
    ], AccountServiceProvider);
    return AccountServiceProvider;
}());

//# sourceMappingURL=account-service.js.map

/***/ }),

/***/ 366:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SignupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_account_service_account_service__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SignupPage = /** @class */ (function () {
    function SignupPage(navCtrl, navParams, viewCtrl, alertCtrl, accountService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.alertCtrl = alertCtrl;
        this.accountService = accountService;
        this.name = "";
        this.email = "";
        this.password = "";
        this.account = { name: '', email: '', password: '' };
    }
    SignupPage.prototype.signup = function () {
        var _this = this;
        this.accountService.signupService(this.account.email, this.account.name, this.account.password).then(function (res) {
            if (res) {
                _this.storeUserCredentials(_this.account.email);
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */]);
            }
            else {
                var alert = _this.alertCtrl.create({
                    title: 'Error',
                    subTitle: "Try again",
                    buttons: ['OK']
                });
                alert.present();
            }
        });
    };
    SignupPage.prototype.back = function () {
        this.navCtrl.pop();
    };
    SignupPage.prototype.useCredentials = function (user) {
        this.isLoggedin = true;
        this.AuthToken = user;
    };
    SignupPage.prototype.storeUserCredentials = function (user) {
        //sessionstorage???
        window.localStorage.setItem('user', user);
        this.useCredentials(user);
    };
    SignupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-signup',template:/*ion-inline-start:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\signup\signup.html"*/'<ion-content padding>\n\n  <div class="center_screen">\n\n      <div class="input_area">\n\n          <ion-icon name="md-person"></ion-icon>\n\n          <input type="text" [(ngModel)]="account.name" placeholder="User Name">\n\n        </div>\n\n    <div class="input_area">\n\n      <ion-icon name="md-mail"></ion-icon>\n\n      <input type="text" [(ngModel)]="account.email" placeholder="E-mail">\n\n    </div>\n\n    <div class="input_area">\n\n      <ion-icon name="md-lock"></ion-icon>\n\n      <input type="password" [(ngModel)]="account.password" placeholder="Password">\n\n    </div>\n\n    <button ion-button full color="primary" round [disabled]="account.name.length < 5 || account.password.length <5" block (tap)="signup()">\n\n      Registrarse\n\n    </button>\n\n    <button ion-button full color="primary" round block (tap)="back()">\n\n        Cancelar\n\n      </button>\n\n      <button ion-button full color="primary" round block (tap)="back()" class="datos">\n\n        Su información está protegida y es <br>\n\n        para uso exclusivo de esta aplicación\n\n    </button>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\signup\signup.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_account_service_account_service__["a" /* AccountServiceProvider */]])
    ], SignupPage);
    return SignupPage;
}());

//# sourceMappingURL=signup.js.map

/***/ }),

/***/ 48:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__cars_cars__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__appointmentList_appointmentList__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__profile_profile__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__appointmentCreate_appointmentCreate__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var TabsPage = /** @class */ (function () {
    function TabsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_1__cars_cars__["a" /* ListCarsPage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_4__appointmentCreate_appointmentCreate__["a" /* AppointmentPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_2__appointmentList_appointmentList__["a" /* AppointmentListPage */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_3__profile_profile__["a" /* ProfilePage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\tabs\tabs.html"*/'<ion-tabs selectedIndex=0>\n\n  <ion-tab [root]="tab1Root" tabIcon="car"></ion-tab>\n\n  <ion-tab [root]="tab2Root" tabIcon="md-calendar"></ion-tab>\n\n  <ion-tab [root]="tab3Root" tabIcon="md-build"></ion-tab>\n\n  <ion-tab [root]="tab4Root" tabIcon="contacts"></ion-tab>\n\n</ion-tabs>\n\n'/*ion-inline-end:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\tabs\tabs.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_5_ionic_angular__["k" /* NavParams */]])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppointmentPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_account_service_account_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_moment__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AppointmentPage = /** @class */ (function () {
    function AppointmentPage(navCtrl, navParams, viewCtrl, alertCtrl, loadingCtrl, accountService, formBuilder) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.accountService = accountService;
        this.formBuilder = formBuilder;
        this.carArray = [];
        this.carList = [];
        this.cars = [];
        this.usersList = [];
        this.users = [];
        this.usersArray = [];
        this.promo = navParams.get('promo');
        console.log(this.promo);
        this.myForm = this.createMyForm();
    }
    AppointmentPage.prototype.loadUserCredentialsStorage = function () {
        var username = window.localStorage.getItem('user');
        return username;
    };
    AppointmentPage.prototype.ionViewWillEnter = function () {
        this.presentLoading();
    };
    AppointmentPage.prototype.createMyForm = function () {
        return this.formBuilder.group({
            car: [''],
            fecha: [''],
            hora: [''],
            tipo: ['']
        });
    };
    //Spinner between two screens
    AppointmentPage.prototype.presentLoading = function () {
        var loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 300
        });
        //after charge, get the Cars
        this.username = this.loadUserCredentialsStorage();
        this.carList = this.carArray;
        this.usersList = this.usersArray;
        this.getUsers();
        this.getCar();
        loader.present();
    };
    AppointmentPage.prototype.doRefresh = function (refresher) {
        this.getUsers().then(function () {
            refresher.complete();
        });
    };
    AppointmentPage.prototype.setAppointment = function () {
        var _this = this;
        var car = this.myForm.get('car').value;
        var fecha = (this.myForm.get('fecha').value).toString();
        var date = __WEBPACK_IMPORTED_MODULE_4_moment___default()(fecha).format('DD-MM-YYYY');
        var hours = (this.myForm.get('hora').value).toString();
        var tipo = this.myForm.get('tipo').value;
        var loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 300
        });
        loader.present().then(function () {
            _this.accountService.setAppointment(hours, date, tipo, car, _this.promo, _this.username)
                .then(function (data) {
                if (data) {
                    var alert = _this.alertCtrl.create({
                        title: "Success",
                        subTitle: "Appointment created correctly",
                        buttons: ['OK']
                    });
                    alert.present();
                }
            });
            _this.myForm.reset();
            _this.navCtrl.parent.select(2);
        });
    };
    AppointmentPage.prototype.initializeCars = function () {
        this.cars = this.carList;
    };
    AppointmentPage.prototype.getItems = function (ev) {
        this.initializeUsers();
        // set val to the value of the searchbar
        var val = ev.target.value;
        // if the value is an empty string don't filter the items
        if (val && val.trim() != '') {
            this.users = this.users.filter(function (item) {
                //With item.name we choose the city name that is a String
                return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
            });
        }
    };
    AppointmentPage.prototype.getCar = function () {
        var _this = this;
        return new Promise(function (resolve) {
            var carsArray = [];
            _this.accountService.getCarsService()
                .subscribe(function (data) {
                var cars = data.json();
                if (cars.length != 0) {
                    for (var _i = 0, cars_1 = cars; _i < cars_1.length; _i++) {
                        var i = cars_1[_i];
                        carsArray.push(i);
                    }
                    _this.carList = carsArray;
                    _this.initializeCars();
                }
            });
            resolve(true);
        });
    };
    AppointmentPage.prototype.initializeUsers = function () {
        this.users = this.usersList;
    };
    AppointmentPage.prototype.getUsers = function () {
        var _this = this;
        return new Promise(function (resolve) {
            var usersArray = [];
            _this.accountService.getListUsers()
                .subscribe(function (data) {
                var users = data.json();
                if (users.length != 0) {
                    for (var _i = 0, users_1 = users; _i < users_1.length; _i++) {
                        var i = users_1[_i];
                        usersArray.push(i);
                    }
                    _this.usersList = usersArray;
                    _this.initializeUsers();
                }
            });
            resolve(true);
        });
    };
    AppointmentPage.prototype.getUser = function (username) {
        var _this = this;
        return new Promise(function (resolve) {
            var usersArray = [];
            _this.accountService.getUser(username)
                .subscribe(function (data) {
                var users = data.json();
                if (users.length != 0) {
                    for (var _i = 0, users_2 = users; _i < users_2.length; _i++) {
                        var i = users_2[_i];
                        usersArray.push(i);
                    }
                    _this.usersList = usersArray;
                    _this.initializeUsers();
                }
            });
            resolve(true);
        });
    };
    AppointmentPage.prototype.deleteUser = function (email) {
        var _this = this;
        var alert;
        this.accountService.deleteUser(email)
            .subscribe(function (data) {
            if (data) {
                alert = _this.alertCtrl.create({
                    title: "HECHO",
                    subTitle: "Usuario de forma correcta",
                    buttons: ['OK']
                });
                _this.getUsers();
                alert.present();
            }
            else {
                alert = _this.alertCtrl.create({
                    title: 'Error',
                    subTitle: "No se ha podido eliminar el usuario",
                    buttons: ['OK']
                });
                alert.present();
            }
        });
    };
    AppointmentPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-appointment',template:/*ion-inline-start:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\appointmentCreate\appointmentCreate.html"*/'<ion-header>\n\n  <ion-toolbar color="primary">\n\n    <ion-buttons left menuToggle>\n\n      <button ion-button icon-only>\n\n        <ion-icon name="menu"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n    <div *ngIf="username!=\'administrator\'">\n\n      <ion-title>{{\'CrearCita\'|translate}}</ion-title>\n\n    </div>\n\n    <div *ngIf="username==\'administrator\'">\n\n      <ion-title>{{\'ListaUsers\'|translate}}</ion-title>\n\n    </div>\n\n  </ion-toolbar>\n\n</ion-header>\n\n\n\n<ion-content fullscreen>\n\n  <div *ngIf="username==\'administrator\'">\n\n    <ion-refresher (ionRefresh)="doRefresh($event)">\n\n      <ion-refresher-content pullingIcon="arrow-dropdown" pullingText="Pull to refresh" refreshingSpinner="circles"\n\n        refreshingText="Refreshing..."></ion-refresher-content>\n\n    </ion-refresher>\n\n    <div class="card-background-page">\n\n      <ion-list no-lines *ngFor="let user of users">\n\n        <ion-item *ngIf="user.name!=\'administrator\'">\n\n          <ion-card>\n\n            <div class="datos">\n\n            <div class="card-title">\n\n              <b>{{user.name}} </b> \n\n            </div>\n\n            <div class="coches">\n\n            <div *ngFor="let car of user.carList" class="card-subtitle">\n\n             {{car.name}}\n\n            </div>\n\n            </div>\n\n          </div>\n\n            <button ion-button full color="primary" icon-start (click)="deleteUser(user.email)">\n\n              <ion-icon name="ios-trash-outline"></ion-icon>\n\n              {{"Eliminar" | translate}}\n\n            </button>\n\n          </ion-card>\n\n        </ion-item>\n\n      </ion-list>\n\n    </div>\n\n  </div>\n\n  <div *ngIf="username!=\'administrator\'" class="form">\n\n    <form [formGroup]="myForm">\n\n      <button *ngIf="promo!=null" ion-button clear color="primary" icon-start class="promo">\n\n        OBTENDRÁ UN {{promo}}% DTO EN SU FACTURA\n\n      </button>\n\n      <ion-list class="lista">\n\n          <div class="etiqueta"><b>{{\'SelecCoche\' | translate}}</b></div>\n\n            <ion-item class="item_inner">\n\n              <ion-select formControlName="car" (ngModelChange)="getCar($event)" placeholder="coche">\n\n                <ion-option *ngFor="let car of cars" [value]="car.carId">\n\n                  {{car.name}}\n\n                </ion-option>\n\n              </ion-select>\n\n            </ion-item>\n\n            <div class="etiqueta"><b>{{\'SelecTipo\' | translate}}</b></div>\n\n            <ion-item class="item_inner" lines="none">\n\n              <ion-icon name=\'md-add-circle\'></ion-icon>\n\n              <ion-select formControlName="tipo" placeholder="tipo">\n\n                <ion-option>Reparacion</ion-option>\n\n                <ion-option>Mantenimiento</ion-option>\n\n              </ion-select>\n\n            </ion-item>\n\n            <div class="etiqueta"><b>{{\'SelecFecha\' | translate}}</b></div>\n\n            <ion-item class="item_inner">\n\n              <ion-datetime displayFormat="DD/MM/YYYY" min="2020" max="2025" formControlName="fecha"\n\n                placeholder="fecha">\n\n              </ion-datetime>\n\n            </ion-item>\n\n            <div class="etiqueta"><b>{{\'SelecHora\' | translate}}</b></div>\n\n            <ion-item class="item_inner">\n\n              <ion-datetime displayFormat="h:mm A" minuteValues="0,30" formControlName="hora" placeholder="hora">\n\n              </ion-datetime>\n\n              \n\n            </ion-item>\n\n      </ion-list>\n\n    </form>\n\n    <div class="horario">\n\n      <ion-card>\n\n        <b>{{"Horario1" | translate}}</b> <br>\n\n        <b>{{"Horario2" | translate}}</b><br>\n\n        <b>09.00h - 18:30h</b>\n\n      </ion-card>\n\n    </div>\n\n  </div>\n\n  \n\n  <div class="bottom" *ngIf="username!=\'administrator\'">\n\n    <button size="large" shape="block" ion-button color="primary" icon-start (click)="setAppointment()">\n\n      <ion-icon name=\'md-add-circle\'></ion-icon>\n\n      Añadir Cita\n\n    </button>\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\appointmentCreate\appointmentCreate.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_account_service_account_service__["a" /* AccountServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */]])
    ], AppointmentPage);
    return AppointmentPage;
}());

//# sourceMappingURL=appointmentCreate.js.map

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListCarsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_account_service_account_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__car_detail_car_detail__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__carcreate_carcreate__ = __webpack_require__(171);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ListCarsPage = /** @class */ (function () {
    function ListCarsPage(navCtrl, navParams, viewCtrl, alertCtrl, loadingCtrl, accountService, nav) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.accountService = accountService;
        this.nav = nav;
        this.carsArray = [];
        this.carsList = [];
        this.cars = [];
        this.carsStateArray = [];
        this.carsStateList = [];
        this.carsState = [];
    }
    ListCarsPage_1 = ListCarsPage;
    ListCarsPage.prototype.loadUserCredentialsStorage = function () {
        var username = window.localStorage.getItem('user');
        return username;
    };
    ListCarsPage.prototype.ionViewWillEnter = function () {
        this.presentLoading();
    };
    ListCarsPage.prototype.presentLoading = function () {
        var loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 30
        });
        //after charge, get the Cities and initialize the list
        this.username = this.loadUserCredentialsStorage();
        this.carsList = this.carsArray;
        this.carsStateList = this.carsStateArray;
        this.getUserCars();
        this.getCarsAdmin();
        loader.present();
    };
    ListCarsPage.prototype.initializeCars = function () {
        this.cars = this.carsList;
    };
    ListCarsPage.prototype.initializeCarsState = function () {
        this.carsState = this.carsStateList;
    };
    ListCarsPage.prototype.getUserCars = function () {
        var _this = this;
        return new Promise(function (resolve) {
            var carsArray = [];
            _this.accountService.getUserCarsService(_this.username)
                .subscribe(function (data) {
                var cars = data.json();
                if (cars.length != 0) {
                    for (var _i = 0, cars_1 = cars; _i < cars_1.length; _i++) {
                        var i = cars_1[_i];
                        carsArray.push(i);
                    }
                    _this.carsList = carsArray;
                    _this.initializeCars();
                }
            });
            resolve(true);
        });
    };
    ListCarsPage.prototype.getCarsAdmin = function () {
        var _this = this;
        return new Promise(function (resolve) {
            var carsStateArray = [];
            _this.accountService.getCarsService()
                .subscribe(function (data) {
                var cars = data.json();
                if (cars.length != 0) {
                    for (var _i = 0, cars_2 = cars; _i < cars_2.length; _i++) {
                        var i = cars_2[_i];
                        for (var _a = 0, _b = i.appointmentList; _a < _b.length; _a++) {
                            var j = _b[_a];
                            if (j.state == "Pendiente")
                                carsStateArray.push(i);
                        }
                        _this.carsStateList = carsStateArray;
                        _this.initializeCarsState();
                    }
                }
            });
            resolve(true);
        });
    };
    ListCarsPage.prototype.getDetail = function (car) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__car_detail_car_detail__["a" /* CarDetailPage */], { car: car });
    };
    ListCarsPage.prototype.saveData = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__carcreate_carcreate__["a" /* CarCreatePage */]);
    };
    ListCarsPage.prototype.deleteCar = function (carId) {
        var _this = this;
        var alert;
        this.accountService.deleteCar(carId)
            .subscribe(function (data) {
            if (data) {
                alert = _this.alertCtrl.create({
                    title: "HECHO",
                    subTitle: "Coche eliminado de forma correcta",
                    buttons: ['OK']
                });
                _this.navCtrl.setRoot(ListCarsPage_1);
                alert.present();
            }
            else {
                alert = _this.alertCtrl.create({
                    title: 'Error',
                    subTitle: "No se ha podido eliminar el coche seleccionado",
                    buttons: ['OK']
                });
                alert.present();
            }
        });
    };
    ListCarsPage = ListCarsPage_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-cars',template:/*ion-inline-start:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\cars\cars.html"*/'<ion-header>\n\n  <ion-toolbar color="primary">\n\n    <ion-buttons left menuToggle>\n\n      <button ion-button icon-only>\n\n        <ion-icon name="menu"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n    <ion-title *ngIf="username!=\'administrator\'">{{"cochepage"|translate}}</ion-title>\n\n    <ion-title *ngIf="username==\'administrator\'">{{"cocheadmin"|translate}}</ion-title>\n\n    <ion-buttons end>\n\n      <div *ngIf="cars.length>=1">\n\n        <button ion-button icon-only (click)="saveData()">\n\n          <ion-icon name="add"></ion-icon>\n\n        </button>\n\n      </div>\n\n    </ion-buttons>\n\n  </ion-toolbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n\n\n  <div *ngIf="cars.length>=1">\n\n    <div *ngIf="username!=\'administrator\'">\n\n      <ion-list no-lines class="lista">\n\n        <ion-item-sliding class="items" (ionInput)="getUserCars($event)" *ngFor="let car of cars">\n\n          <ion-item (tap)="getDetail(car)" class="lista_coches">\n\n              <div class="coche_name">\n\n                <div class="nombres">\n\n                <h3>{{car.name}}</h3>\n\n                <h2>{{car.matricula}}</h2>\n\n              </div>\n\n              <button ion-button full icon-start class="detalle">\n\n                <ion-icon name="md-eye"></ion-icon>\n\n                Ver detalle\n\n              </button>\n\n            </div>\n\n          </ion-item>\n\n          <ion-item-options side="left" class="options">\n\n            <div *ngIf="username!=\'administrator\'">\n\n            <button ion-button color="danger" (click)="deleteCar(car.carId)">\n\n              <ion-icon name="md-trash"></ion-icon>\n\n            </button>\n\n          </div>\n\n          </ion-item-options>\n\n        </ion-item-sliding>\n\n      </ion-list>\n\n    </div>\n\n  </div>\n\n\n\n  <div *ngIf="cars.length==0">\n\n    <div *ngIf="username!=\'administrator\'">\n\n      <img class="foto" src="../assets/imgs/successful.png">\n\n      <ion-title text-center class="texto">{{"Coche1"|translate}}</ion-title>\n\n      <button class="boton2" ion-button icon-only (click)="saveData()">\n\n        {{"Coche2"|translate}}\n\n      </button>\n\n    </div>\n\n  </div>\n\n  <div *ngIf="carsState.length>=1">\n\n    <div *ngIf="username==\'administrator\'">\n\n      <ion-list no-lines class="lista">\n\n        <ion-item-sliding class="items"  (ionInput)="getCarsAdmin($event)" *ngFor="let carSt of carsState" class="items">\n\n          <ion-item class="lista_coches" (tap)="getDetail(carSt)">\n\n            <div class="coche_name">\n\n              <div class="nombres">\n\n              <h3>{{carSt.name}}</h3>\n\n              <h2>{{carSt.matricula}}</h2>\n\n            </div>\n\n            <button ion-button full icon-start class="detalle">\n\n              <ion-icon name="md-eye"></ion-icon>\n\n              Ver detalle\n\n            </button>\n\n          </div>\n\n          </ion-item>\n\n        </ion-item-sliding>\n\n      </ion-list>\n\n    </div>\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\cars\cars.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_account_service_account_service__["a" /* AccountServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Nav */]])
    ], ListCarsPage);
    return ListCarsPage;
    var ListCarsPage_1;
}());

//# sourceMappingURL=cars.js.map

/***/ }),

/***/ 510:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WelcomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__ = __webpack_require__(48);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the InitPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var WelcomePage = /** @class */ (function () {
    function WelcomePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    WelcomePage.prototype.ionViewDidLoad = function () {
    };
    WelcomePage.prototype.gotologin = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */]);
    };
    WelcomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-init',template:/*ion-inline-start:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\welcome\welcome.html"*/'<ion-content>\n\n  <div class="slider_area">\n\n    <ion-slides pager [autoplay]="2500">\n\n      <ion-slide>\n\n        <div class="slider_content">\n\n          <img class="logo" src="assets/imgs/logo.svg">\n\n          <h3>Prueba 1</h3>\n\n          <small>Pruebita 1</small>\n\n        </div>\n\n      </ion-slide>\n\n\n\n      <ion-slide>\n\n        <div class="slider_content">\n\n          <img class="specification_img" src="assets/imgs/acceptfriend.svg">\n\n          <h3>Prueba 2</h3>\n\n          <small>Pruebita 2</small>\n\n        </div>\n\n      </ion-slide>\n\n\n\n      <ion-slide>\n\n        <div class="slider_content">\n\n          <img class="specification_img" src="assets/imgs/chat.svg">\n\n          <h3>Prueba 3</h3>\n\n          <small>Pruebita 3</small>\n\n        </div>\n\n      </ion-slide>\n\n\n\n    </ion-slides>\n\n  </div>\n\n  <div class="button_section">\n\n    <button block ion-button color="primary" outline round (tap)="gotologin()">EMPECEMOS</button>\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\welcome\welcome.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */]])
    ], WelcomePage);
    return WelcomePage;
}());

//# sourceMappingURL=welcome.js.map

/***/ }),

/***/ 511:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(512);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(516);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 516:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export createTranslateLoader */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(42);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(572);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_about_about__ = __webpack_require__(837);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_contact_contact__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_tabs_tabs__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_cars_cars__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_language_language__ = __webpack_require__(174);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_email_composer__ = __webpack_require__(362);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_file__ = __webpack_require__(838);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_file_opener__ = __webpack_require__(839);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_common_http__ = __webpack_require__(840);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_http__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_status_bar__ = __webpack_require__(422);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_splash_screen__ = __webpack_require__(423);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_welcome_welcome__ = __webpack_require__(510);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_login_login__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__providers_account_service_account_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_signup_signup__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_car_detail_car_detail__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_carcreate_carcreate__ = __webpack_require__(171);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_profile_profile__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_appointmentCreate_appointmentCreate__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_appointmentList_appointmentList__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ngx_translate_http_loader__ = __webpack_require__(841);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ngx_translate_core__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_bill_bill__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_forgot_password_forgot_password__ = __webpack_require__(173);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_filters_filters__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__ionic_native_push_ngx__ = __webpack_require__(424);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};































function createTranslateLoader(http) {
    return new __WEBPACK_IMPORTED_MODULE_25__ngx_translate_http_loader__["a" /* TranslateHttpLoader */](http, './assets/i18n/', '.json');
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_welcome_welcome__["a" /* WelcomePage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_signup_signup__["a" /* SignupPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_cars_cars__["a" /* ListCarsPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_car_detail_car_detail__["a" /* CarDetailPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_carcreate_carcreate__["a" /* CarCreatePage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_appointmentCreate_appointmentCreate__["a" /* AppointmentPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_appointmentList_appointmentList__["a" /* AppointmentListPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_language_language__["a" /* LanguagePage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_bill_bill__["a" /* BillPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_forgot_password_forgot_password__["a" /* ForgotPasswordPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_filters_filters__["a" /* FiltersPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_13__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_12__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/appointmentCreate/appointmentCreate.module#AppointmentPageModule', name: 'AppointmentPage', segment: 'appointmentCreate', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/appointmentList/appointmentList.module#AppointmentPageModule', name: 'AppointmentListPage', segment: 'appointmentList', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/bill/bill.module#BillPageModule', name: 'BillPage', segment: 'bill', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/car-detail/car-detail.module#CarDetailPageModule', name: 'CarDetailPage', segment: 'car-detail', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/carcreate/carcreate.module#CarcreatePageModule', name: 'CarCreatePage', segment: 'carcreate', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/cars/cars.module#CarsPageModule', name: 'ListCarsPage', segment: 'cars', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/contact/contact.module#ContactPageModule', name: 'ContactPage', segment: 'contact', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/filters/filters.module#FiltersPageModule', name: 'FiltersPage', segment: 'filters', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/forgot-password/forgot-password.module#ForgotPasswordPageModule', name: 'ForgotPasswordPage', segment: 'forgot-password', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/language/language.module#LanguagePageModule', name: 'LanguagePage', segment: 'language', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/profile/profile.module#ProfilePageModule', name: 'ProfilePage', segment: 'profile', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/welcome/welcome.module#WelcomePageModule', name: 'WelcomePage', segment: 'welcome', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_26__ngx_translate_core__["b" /* TranslateModule */].forRoot({
                    loader: {
                        provide: __WEBPACK_IMPORTED_MODULE_26__ngx_translate_core__["a" /* TranslateLoader */],
                        useFactory: (createTranslateLoader),
                        deps: [__WEBPACK_IMPORTED_MODULE_12__angular_common_http__["a" /* HttpClient */]]
                    }
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_welcome_welcome__["a" /* WelcomePage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_signup_signup__["a" /* SignupPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_cars_cars__["a" /* ListCarsPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_car_detail_car_detail__["a" /* CarDetailPage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_carcreate_carcreate__["a" /* CarCreatePage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_appointmentCreate_appointmentCreate__["a" /* AppointmentPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_appointmentList_appointmentList__["a" /* AppointmentListPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_language_language__["a" /* LanguagePage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_bill_bill__["a" /* BillPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_forgot_password_forgot_password__["a" /* ForgotPasswordPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_filters_filters__["a" /* FiltersPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_15__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_30__ionic_native_push_ngx__["a" /* Push */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_18__providers_account_service_account_service__["a" /* AccountServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_10__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_11__ionic_native_file_opener__["a" /* FileOpener */],
                __WEBPACK_IMPORTED_MODULE_9__ionic_native_email_composer__["a" /* EmailComposer */] //añadido para generar el email
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 536:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return URL_SERVICIOS; });
var URL_SERVICIOS = "http://192.168.1.28:8080";
//# sourceMappingURL=urlService.js.map

/***/ }),

/***/ 538:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./af": 230,
	"./af.js": 230,
	"./ar": 231,
	"./ar-dz": 232,
	"./ar-dz.js": 232,
	"./ar-kw": 233,
	"./ar-kw.js": 233,
	"./ar-ly": 234,
	"./ar-ly.js": 234,
	"./ar-ma": 235,
	"./ar-ma.js": 235,
	"./ar-sa": 236,
	"./ar-sa.js": 236,
	"./ar-tn": 237,
	"./ar-tn.js": 237,
	"./ar.js": 231,
	"./az": 238,
	"./az.js": 238,
	"./be": 239,
	"./be.js": 239,
	"./bg": 240,
	"./bg.js": 240,
	"./bm": 241,
	"./bm.js": 241,
	"./bn": 242,
	"./bn.js": 242,
	"./bo": 243,
	"./bo.js": 243,
	"./br": 244,
	"./br.js": 244,
	"./bs": 245,
	"./bs.js": 245,
	"./ca": 246,
	"./ca.js": 246,
	"./cs": 247,
	"./cs.js": 247,
	"./cv": 248,
	"./cv.js": 248,
	"./cy": 249,
	"./cy.js": 249,
	"./da": 250,
	"./da.js": 250,
	"./de": 251,
	"./de-at": 252,
	"./de-at.js": 252,
	"./de-ch": 253,
	"./de-ch.js": 253,
	"./de.js": 251,
	"./dv": 254,
	"./dv.js": 254,
	"./el": 255,
	"./el.js": 255,
	"./en-au": 256,
	"./en-au.js": 256,
	"./en-ca": 257,
	"./en-ca.js": 257,
	"./en-gb": 258,
	"./en-gb.js": 258,
	"./en-ie": 259,
	"./en-ie.js": 259,
	"./en-il": 260,
	"./en-il.js": 260,
	"./en-in": 261,
	"./en-in.js": 261,
	"./en-nz": 262,
	"./en-nz.js": 262,
	"./en-sg": 263,
	"./en-sg.js": 263,
	"./eo": 264,
	"./eo.js": 264,
	"./es": 265,
	"./es-do": 266,
	"./es-do.js": 266,
	"./es-us": 267,
	"./es-us.js": 267,
	"./es.js": 265,
	"./et": 268,
	"./et.js": 268,
	"./eu": 269,
	"./eu.js": 269,
	"./fa": 270,
	"./fa.js": 270,
	"./fi": 271,
	"./fi.js": 271,
	"./fil": 272,
	"./fil.js": 272,
	"./fo": 273,
	"./fo.js": 273,
	"./fr": 274,
	"./fr-ca": 275,
	"./fr-ca.js": 275,
	"./fr-ch": 276,
	"./fr-ch.js": 276,
	"./fr.js": 274,
	"./fy": 277,
	"./fy.js": 277,
	"./ga": 278,
	"./ga.js": 278,
	"./gd": 279,
	"./gd.js": 279,
	"./gl": 280,
	"./gl.js": 280,
	"./gom-deva": 281,
	"./gom-deva.js": 281,
	"./gom-latn": 282,
	"./gom-latn.js": 282,
	"./gu": 283,
	"./gu.js": 283,
	"./he": 284,
	"./he.js": 284,
	"./hi": 285,
	"./hi.js": 285,
	"./hr": 286,
	"./hr.js": 286,
	"./hu": 287,
	"./hu.js": 287,
	"./hy-am": 288,
	"./hy-am.js": 288,
	"./id": 289,
	"./id.js": 289,
	"./is": 290,
	"./is.js": 290,
	"./it": 291,
	"./it-ch": 292,
	"./it-ch.js": 292,
	"./it.js": 291,
	"./ja": 293,
	"./ja.js": 293,
	"./jv": 294,
	"./jv.js": 294,
	"./ka": 295,
	"./ka.js": 295,
	"./kk": 296,
	"./kk.js": 296,
	"./km": 297,
	"./km.js": 297,
	"./kn": 298,
	"./kn.js": 298,
	"./ko": 299,
	"./ko.js": 299,
	"./ku": 300,
	"./ku.js": 300,
	"./ky": 301,
	"./ky.js": 301,
	"./lb": 302,
	"./lb.js": 302,
	"./lo": 303,
	"./lo.js": 303,
	"./lt": 304,
	"./lt.js": 304,
	"./lv": 305,
	"./lv.js": 305,
	"./me": 306,
	"./me.js": 306,
	"./mi": 307,
	"./mi.js": 307,
	"./mk": 308,
	"./mk.js": 308,
	"./ml": 309,
	"./ml.js": 309,
	"./mn": 310,
	"./mn.js": 310,
	"./mr": 311,
	"./mr.js": 311,
	"./ms": 312,
	"./ms-my": 313,
	"./ms-my.js": 313,
	"./ms.js": 312,
	"./mt": 314,
	"./mt.js": 314,
	"./my": 315,
	"./my.js": 315,
	"./nb": 316,
	"./nb.js": 316,
	"./ne": 317,
	"./ne.js": 317,
	"./nl": 318,
	"./nl-be": 319,
	"./nl-be.js": 319,
	"./nl.js": 318,
	"./nn": 320,
	"./nn.js": 320,
	"./oc-lnc": 321,
	"./oc-lnc.js": 321,
	"./pa-in": 322,
	"./pa-in.js": 322,
	"./pl": 323,
	"./pl.js": 323,
	"./pt": 324,
	"./pt-br": 325,
	"./pt-br.js": 325,
	"./pt.js": 324,
	"./ro": 326,
	"./ro.js": 326,
	"./ru": 327,
	"./ru.js": 327,
	"./sd": 328,
	"./sd.js": 328,
	"./se": 329,
	"./se.js": 329,
	"./si": 330,
	"./si.js": 330,
	"./sk": 331,
	"./sk.js": 331,
	"./sl": 332,
	"./sl.js": 332,
	"./sq": 333,
	"./sq.js": 333,
	"./sr": 334,
	"./sr-cyrl": 335,
	"./sr-cyrl.js": 335,
	"./sr.js": 334,
	"./ss": 336,
	"./ss.js": 336,
	"./sv": 337,
	"./sv.js": 337,
	"./sw": 338,
	"./sw.js": 338,
	"./ta": 339,
	"./ta.js": 339,
	"./te": 340,
	"./te.js": 340,
	"./tet": 341,
	"./tet.js": 341,
	"./tg": 342,
	"./tg.js": 342,
	"./th": 343,
	"./th.js": 343,
	"./tl-ph": 344,
	"./tl-ph.js": 344,
	"./tlh": 345,
	"./tlh.js": 345,
	"./tr": 346,
	"./tr.js": 346,
	"./tzl": 347,
	"./tzl.js": 347,
	"./tzm": 348,
	"./tzm-latn": 349,
	"./tzm-latn.js": 349,
	"./tzm.js": 348,
	"./ug-cn": 350,
	"./ug-cn.js": 350,
	"./uk": 351,
	"./uk.js": 351,
	"./ur": 352,
	"./ur.js": 352,
	"./uz": 353,
	"./uz-latn": 354,
	"./uz-latn.js": 354,
	"./uz.js": 353,
	"./vi": 355,
	"./vi.js": 355,
	"./x-pseudo": 356,
	"./x-pseudo.js": 356,
	"./yo": 357,
	"./yo.js": 357,
	"./zh-cn": 358,
	"./zh-cn.js": 358,
	"./zh-hk": 359,
	"./zh-hk.js": 359,
	"./zh-mo": 360,
	"./zh-mo.js": 360,
	"./zh-tw": 361,
	"./zh-tw.js": 361
};
function webpackContext(req) {
	return __webpack_require__(webpackContextResolve(req));
};
function webpackContextResolve(req) {
	var id = map[req];
	if(!(id + 1)) // check for number or string
		throw new Error("Cannot find module '" + req + "'.");
	return id;
};
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = 538;

/***/ }),

/***/ 572:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(422);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(423);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_account_service_account_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_cars_cars__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_tabs_tabs__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_profile_profile__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_language_language__ = __webpack_require__(174);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ngx_translate_core__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_contact_contact__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_appointmentList_appointmentList__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_appointmentCreate_appointmentCreate__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_push_ngx__ = __webpack_require__(424);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
















var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, menuCtrl, loadingCtrl, accountService, translate, push) {
        var _this = this;
        this.menuCtrl = menuCtrl;
        this.loadingCtrl = loadingCtrl;
        this.accountService = accountService;
        this.translate = translate;
        this.push = push;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */];
        var language = translate.getBrowserLang();
        this.translate.setDefaultLang(language);
        this.translate.use(language);
        platform.ready().then(function () {
            statusBar.styleDefault();
            splashScreen.hide();
            _this.pushSetup();
        });
        this.pages = [
            { title: 'Coche', component: __WEBPACK_IMPORTED_MODULE_6__pages_cars_cars__["a" /* ListCarsPage */] },
            { title: 'Profile', component: __WEBPACK_IMPORTED_MODULE_8__pages_profile_profile__["a" /* ProfilePage */] },
            { title: 'Citas', component: __WEBPACK_IMPORTED_MODULE_12__pages_appointmentList_appointmentList__["a" /* AppointmentListPage */] },
            { title: 'Crear cita', component: __WEBPACK_IMPORTED_MODULE_13__pages_appointmentCreate_appointmentCreate__["a" /* AppointmentPage */] }
        ];
    }
    MyApp.prototype.pushSetup = function () {
        var options = {
            android: {
                // Añadimos el sender ID para Android.
                senderID: '560790819032'
            },
            ios: {
                alert: 'true',
                badge: true,
                sound: 'false'
            }
        };
        var pushObject = this.push.init(options);
        pushObject.on('notification').subscribe(function (notification) { return console.log('Received a notification', notification); });
        pushObject.on('registration').subscribe(function (registration) { return console.log('Device registered', registration); });
        pushObject.on('error').subscribe(function (error) { return console.error('Error with Push plugin', error); });
    };
    MyApp.prototype.Coches = function () {
        this.menuCtrl.close();
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_tabs_tabs__["a" /* TabsPage */]);
    };
    MyApp.prototype.logout = function () {
        this.menuCtrl.close();
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */]);
    };
    MyApp.prototype.languages = function () {
        this.menuCtrl.close();
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_9__pages_language_language__["a" /* LanguagePage */]);
    };
    MyApp.prototype.contact = function () {
        this.menuCtrl.close();
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_11__pages_contact_contact__["a" /* ContactPage */]);
    };
    MyApp.prototype.Profile = function () {
        this.menuCtrl.close();
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_8__pages_profile_profile__["a" /* ProfilePage */]);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\app\app.html"*/'<ion-menu side="left" [content]="content" persistent="true">\n\n    <ion-header>\n\n        <ion-toolbar color="primary">\n\n              <button ion-button menuToggle>\n\n                <ion-icon name="menu"></ion-icon>\n\n              </button>\n\n            <ion-toolbar>\n\n                <ion-title>{{"Opciones" | translate}}</ion-title>\n\n            </ion-toolbar>\n\n        </ion-toolbar>\n\n    </ion-header>\n\n<ion-content>\n\n    <ion-list>\n\n        <button ion-item (click)="Coches()">\n\n            <ion-icon name="ios-car-outline"></ion-icon>\n\n            Lista de Coches\n\n        </button>\n\n        <button ion-item (click)="contact()">\n\n            <ion-icon name="ios-mail-outline"></ion-icon>\n\n            {{"contact" | translate}}\n\n        </button>\n\n        <button ion-item (click)="languages()">\n\n            <ion-icon name="ios-globe-outline"></ion-icon>\n\n            {{"languages" | translate }}\n\n        </button>\n\n        <button ion-item (click)="logout()">\n\n          <ion-icon name="ios-log-out"></ion-icon>\n\n            {{"logout" | translate}}\n\n        </button>\n\n    </ion-list>\n\n</ion-content>\n\n</ion-menu>\n\n<ion-nav id="nav" #content [root]="rootPage"></ion-nav>\n\n'/*ion-inline-end:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_account_service_account_service__["a" /* AccountServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_10__ngx_translate_core__["c" /* TranslateService */],
            __WEBPACK_IMPORTED_MODULE_14__ionic_native_push_ngx__["a" /* Push */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 70:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppointmentListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_account_service_account_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_forms__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__appointmentCreate_appointmentCreate__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_pdfmake_build_pdfmake__ = __webpack_require__(539);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_pdfmake_build_pdfmake___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_pdfmake_build_pdfmake__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_pdfmake_build_vfs_fonts__ = __webpack_require__(542);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_pdfmake_build_vfs_fonts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_pdfmake_build_vfs_fonts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__bill_bill__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_moment__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_moment___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_moment__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__filters_filters__ = __webpack_require__(169);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









__WEBPACK_IMPORTED_MODULE_6_pdfmake_build_pdfmake___default.a.vfs = __WEBPACK_IMPORTED_MODULE_7_pdfmake_build_vfs_fonts___default.a.pdfMake.vfs;


var AppointmentListPage = /** @class */ (function () {
    function AppointmentListPage(navCtrl, http, plt, navParams, formBuilder, alertCtrl, loadingCtrl, events, accountServiceProvider, menuCtrl) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.http = http;
        this.plt = plt;
        this.navParams = navParams;
        this.formBuilder = formBuilder;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.events = events;
        this.accountServiceProvider = accountServiceProvider;
        this.menuCtrl = menuCtrl;
        this.cars = [];
        this.carList = [];
        this.carArray = [];
        this.appointmentList = [];
        this.appointmentArray = [];
        this.appointments = [];
        this.pdfObj = null;
        /************FILTER***************/
        this.state = null;
        this.tipo = null;
        this.filtrado = false;
        this.myForm = this.createMyForm();
        this.events.subscribe('state:tipo', function (state, tipo) {
            _this.state = state;
            _this.tipo = tipo;
        });
    }
    AppointmentListPage.prototype.loadUserCredentialsStorage = function () {
        var username = window.localStorage.getItem('user');
        return username;
    };
    AppointmentListPage.prototype.ionViewWillEnter = function () {
        this.presentLoading();
    };
    //Spinner between two screens
    AppointmentListPage.prototype.presentLoading = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 300
        });
        //after charge, get the cars and initialize the list
        this.username = this.loadUserCredentialsStorage();
        this.cars = this.carList;
        this.appointments = this.appointmentList;
        this.getCars();
        if (this.appointments != null && this.appointments.length > 1
            && (this.state != null || this.tipo != null)) {
            loader.present().then(function () {
                _this.applyFilters(_this.state, _this.tipo).then(function () {
                    loader.dismiss();
                });
            });
        }
    };
    AppointmentListPage.prototype.createMyForm = function () {
        return this.formBuilder.group({
            estado: ['', __WEBPACK_IMPORTED_MODULE_4__angular_forms__["g" /* Validators */].required]
        });
    };
    AppointmentListPage.prototype.setState = function (appointmentId) {
        var _this = this;
        var estado = this.myForm.get('estado').value;
        var loader = this.loadingCtrl.create({
            content: "Please wait...",
            duration: 300
        });
        loader.present().then(function () {
            _this.accountServiceProvider.updateAppointment(appointmentId, estado)
                .then(function (data) { });
        });
    };
    AppointmentListPage.prototype.deleteAppointment = function (appointmentId) {
        var _this = this;
        var alert;
        this.accountServiceProvider.deleteAppointment(appointmentId)
            .subscribe(function (data) {
            if (data) {
                alert = _this.alertCtrl.create({
                    title: "HECHO",
                    subTitle: "Cita eliminada de forma correcta",
                    buttons: ['OK']
                });
                _this.presentLoading();
                alert.present();
            }
            else {
                alert = _this.alertCtrl.create({
                    title: 'Error',
                    subTitle: "No se ha podido eliminar la cita seleccionada",
                    buttons: ['OK']
                });
                alert.present();
            }
        });
    };
    AppointmentListPage.prototype.addBill = function (appointment) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__bill_bill__["a" /* BillPage */], { appointment: appointment });
    };
    AppointmentListPage.prototype.deleteBill = function (billId) {
        var _this = this;
        this.accountServiceProvider.deleteBill(billId)
            .subscribe(function (data) {
            if (data) {
                var alert = _this.alertCtrl.create({
                    title: "Success",
                    subTitle: "Bill deleted from Appointment",
                    buttons: ['OK']
                });
                _this.presentLoading();
                alert.present();
            }
            else {
                var alert = _this.alertCtrl.create({
                    title: 'Error',
                    subTitle: "{{ 'please' | translate }}",
                    buttons: ['OK']
                });
                alert.present();
            }
        });
        ;
    };
    AppointmentListPage.prototype.initializeCars = function () {
        this.cars = this.carList;
        this.appointments = this.appointmentList;
    };
    AppointmentListPage.prototype.getCars = function () {
        var _this = this;
        var carsArray = [];
        var appointmentArray = [];
        this.accountServiceProvider.getCarsService()
            .subscribe(function (data) {
            var cars = data.json();
            if (cars.length != 0) {
                for (var _i = 0, cars_1 = cars; _i < cars_1.length; _i++) {
                    var i = cars_1[_i];
                    for (var _a = 0, _b = i.appointmentList; _a < _b.length; _a++) {
                        var j = _b[_a];
                        appointmentArray.push(j);
                    }
                    carsArray.push(i);
                }
                _this.appointmentList = appointmentArray;
                _this.carList = carsArray;
                _this.initializeCars();
            }
        });
    };
    AppointmentListPage.prototype.saveData = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__appointmentCreate_appointmentCreate__["a" /* AppointmentPage */]);
    };
    /************************FILTERS*******************************/
    AppointmentListPage.prototype.filters = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_10__filters_filters__["a" /* FiltersPage */]);
    };
    AppointmentListPage.prototype.applyFilters = function (state, tipo) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.filtrado = false;
            _this.initializeCars();
            _this.appointments = _this.appointments.filter(function (item) {
                if (state != null && tipo != null) {
                    _this.filtrado = true;
                    return (item.state == state && item.tipo == tipo);
                }
                else if (state == null && tipo != null) {
                    _this.filtrado = true;
                    return (item.tipo == _this.tipo);
                }
                else if (state != null && tipo == null) {
                    _this.filtrado = true;
                    return (item.state == _this.state);
                }
            });
            resolve(true);
        });
    };
    AppointmentListPage.prototype.cleanFilters = function () {
        this.tipo = null;
        this.state = null;
        this.initializeCars();
    };
    AppointmentListPage.prototype.createPdf = function (bill) {
        //recordemos que para una cita hay solo una factura OneToOne relationship
        var docDefinition = {
            content: [
                { text: 'CarAuto', style: 'header' },
                { text: __WEBPACK_IMPORTED_MODULE_9_moment___default()(Date.now()).format('DD-MM-YYYY'), alignment: 'right' },
                { text: 'From', style: 'subheader' },
                "Cochify",
                "Direccion: Calle ",
                "Pais: España",
                { text: 'To', style: 'subheader' },
                "Noelia Cotelo",
                "Direccion: Malpica",
                "Pais: España",
                { text: 'Desglose', style: 'subheader' },
                {
                    style: 'itemsTable',
                    table: {
                        widths: ['*', 75, 75],
                        body: [
                            [
                                { text: 'Description', style: 'itemsTableHeader' },
                                { text: 'Quantity', style: 'itemsTableHeader' },
                                { text: 'Price', style: 'itemsTableHeader' },
                            ],
                            [
                                bill.description, bill.quantity, bill.importe
                            ],
                            [
                                { text: 'Promociones', style: 'itemsTableHeader' },
                                { text: 'Quantity', style: 'itemsTableHeader' },
                                { text: 'Price', style: 'itemsTableHeader' },
                            ],
                            [
                                bill.description, bill.quantity, bill.importe
                            ]
                        ]
                    }
                },
                {
                    style: 'totalsTable',
                    table: {
                        widths: ['*', 75, 75],
                        body: [
                            [
                                '',
                                'Subtotal',
                                bill.importe,
                            ],
                            [
                                '',
                                'IVA (21%)',
                                bill.iva,
                            ],
                            [
                                '',
                                'Promociones',
                                bill.total,
                            ],
                            [
                                '',
                                'Total (€)',
                                bill.total,
                            ]
                        ]
                    },
                    layout: 'noBorders'
                },
            ],
            styles: {
                header: {
                    fontSize: 20,
                    bold: true,
                    margin: [0, 0, 0, 10],
                    alignment: 'right'
                },
                subheader: {
                    fontSize: 16,
                    bold: true,
                    margin: [0, 20, 0, 5]
                },
                itemsTable: {
                    margin: [0, 5, 0, 15]
                },
                itemsTableHeader: {
                    bold: true,
                    fontSize: 13,
                    color: 'black'
                },
                totalsTable: {
                    bold: true,
                    margin: [0, 30, 0, 0]
                }
            },
            defaultStyle: {}
        };
        this.pdfObj = __WEBPACK_IMPORTED_MODULE_6_pdfmake_build_pdfmake___default.a.createPdf(docDefinition);
    };
    AppointmentListPage.prototype.downloadPdf = function (appointment) {
        this.createPdf(appointment.bill); //cuando clicka en descargar, primero general el pdf
        if (this.plt.is('cordova')) {
            //abre el pdf en caso de ser nativo
            this.pdfObj.open();
        }
        else {
            // On a browser simply use download!
            this.pdfObj.download(); //una vez generado el pdf con el primer metodo (linea 229), ejecutal la descarga.
            //download es un metodo predefinidom de la libreria para pdfs de Ionic
        }
    };
    AppointmentListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-repairs',template:/*ion-inline-start:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\appointmentList\AppointmentList.html"*/'<ion-header>\n\n  <ion-toolbar color="primary">\n\n    <ion-buttons left menuToggle>\n\n      <button ion-button icon-only>\n\n        <ion-icon name="menu"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n    <ion-title>{{"ListaCitas" | translate}}</ion-title>\n\n  </ion-toolbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n\n\n  <div *ngIf="appointments.length==0">\n\n    <div *ngIf="username!=\'administrator\'">\n\n      <img class="foto" src="../assets/imgs/calendario.png">\n\n      <ion-title text-center class="texto">{{"Cita1"|translate}}</ion-title>\n\n      <button class="boton2" ion-button icon-only (click)="saveData()">\n\n        {{"Cita2"|translate}}\n\n      </button>\n\n    </div>\n\n  </div>\n\n  <div *ngIf="appointments.length>0">\n\n    <button ion-button clear color="primary" icon-start (tap)="filters()">\n\n      <ion-icon name=\'ios-funnel\'></ion-icon>\n\n      {{"filters" | translate }}\n\n    </button>\n\n    <button ion-button clear color="primary" style="float:right" icon-start (tap)="cleanFilters()">\n\n      <ion-icon name=\'ios-backspace-outline\'></ion-icon>\n\n      {{"clean" | translate }}\n\n    </button>\n\n    <form [formGroup]="myForm">\n\n      <ion-list no-lines class="lista">\n\n        <ion-item class="items" (ionInput)="getCars($event)" *ngFor="let car of cars">\n\n          <div (ionInput)="getAppointment($event)" *ngFor="let appointment of car.appointmentList" class="item_inner d-flex">\n\n            <div class="date_time">\n\n              <h3>{{appointment.date}}</h3>\n\n              <h4>{{appointment.hours}} h</h4>\n\n              <h5 class="d-flex">\n\n                <ion-icon class="zmdi zmdi-pin ion-text-start"></ion-icon>\n\n                {{appointment.tipo}}\n\n              </h5>\n\n            </div>\n\n            <div class="appointment_details">\n\n              <div class="bill">\n\n                <h3>{{car.name}}</h3>\n\n                <button *ngIf="username==\'administrator\'" ion-button clear small color="item1" icon-start\n\n                  (click)="addBill(appointment)">\n\n                  <ion-icon name=\'md-clipboard\'></ion-icon>\n\n                </button>\n\n                <button *ngIf="username==\'administrator\'" ion-button icon-left clear small color="primary"\n\n                  (tap)="deleteAppointment(appointment.appointmentId)">\n\n                  <ion-icon name=\'ios-trash-outline\'></ion-icon>\n\n                </button>\n\n                <button *ngIf="username!=\'administrator\' && appointment.bill != null" ion-button clear small\n\n                  color="item1" icon-start (click)="downloadPdf(appointment)">\n\n                    <ion-icon name="md-download" size="small" slot="start"></ion-icon>\n\n                    Factura\n\n                </button>\n\n              </div>\n\n              <h4 *ngIf="username!=\'administrator\'">{{appointment.state}}</h4>\n\n              <div class="detalles">\n\n                <div *ngIf="username==\'administrator\'" class="administrator_details">\n\n                  <ion-item class="boton">\n\n                    <ion-select formControlName="estado" placeholder="{{appointment.state}}">\n\n                      <ion-option>Pendiente</ion-option>\n\n                      <ion-option>En curso</ion-option>\n\n                      <ion-option>Finalizado</ion-option>\n\n                      <ion-option>Cancelado</ion-option>\n\n                    </ion-select>\n\n                  </ion-item>\n\n                </div>\n\n                <div *ngIf="username==\'administrator\'" class="buttom_details">\n\n                  <button ion-button icon-left color="primary" [disabled]="!myForm.valid" icon-start\n\n                    (click)="setState(appointment.appointmentId)" class="guardar">\n\n                    <ion-icon name="md-checkmark"></ion-icon>\n\n                    Guardar\n\n                  </button>\n\n                </div>\n\n              </div>\n\n            </div>\n\n          </div>\n\n        </ion-item>\n\n      </ion-list>\n\n    </form>\n\n  </div>\n\n</ion-content>'/*ion-inline-end:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\appointmentList\AppointmentList.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* Events */],
            __WEBPACK_IMPORTED_MODULE_3__providers_account_service_account_service__["a" /* AccountServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* MenuController */]])
    ], AppointmentListPage);
    return AppointmentListPage;
}());

//# sourceMappingURL=appointmentList.js.map

/***/ }),

/***/ 82:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__signup_signup__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_account_service_account_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__tabs_tabs__ = __webpack_require__(48);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__forgot_password_forgot_password__ = __webpack_require__(173);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, viewCtrl, alertCtrl, accountService) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.alertCtrl = alertCtrl;
        this.accountService = accountService;
        this.user = "";
        this.password = "";
        this.account = { user: '', password: '' };
    }
    //Login with email and password
    LoginPage.prototype.login = function () {
        var _this = this;
        this.accountService.loginService(this.account.user, this.account.password).then(function (res) {
            if (res) {
                _this.storeUserCredentials(_this.account.user);
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__tabs_tabs__["a" /* TabsPage */]);
            }
            else {
                var alert = _this.alertCtrl.create({
                    title: 'Error',
                    subTitle: "Try again",
                    buttons: ['OK']
                });
                alert.present();
            }
        });
        ;
    };
    LoginPage.prototype.useCredentials = function (user) {
        this.isLoggedin = true;
        this.AuthToken = user;
    };
    LoginPage.prototype.storeUserCredentials = function (user) {
        //sessionstorage???
        window.localStorage.setItem('user', user);
        this.useCredentials(user);
    };
    /* Voy a la página de sign up para rellenar el formulario*/
    /*Con este push, si quiero volver atrás (página inicial) con un pop*/
    LoginPage.prototype.toSingUp = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__signup_signup__["a" /* SignupPage */]);
    };
    LoginPage.prototype.gotoforgotpassword = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__forgot_password_forgot_password__["a" /* ForgotPasswordPage */]);
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\login\login.html"*/'<ion-content padding>\n\n  <div class="center_screen">\n\n    <div class="input_area">\n\n      <ion-icon name="md-person"></ion-icon>\n\n      <input type="text" [(ngModel)]="account.user" placeholder="User Name">\n\n    </div>\n\n    <div class="input_area">\n\n      <ion-icon name="md-lock"></ion-icon>\n\n      <input type="password" [(ngModel)]="account.password" placeholder="Password">\n\n    </div>\n\n    <button ion-button full color="primary" round [disabled]="account.user.length < 5 || account.password.length <5" block (tap)="login()">\n\n      Login\n\n    </button>\n\n    <div class="forgotpassword_section">\n\n      <button ion-button icon-left clear small color=extralight (tap)="gotoforgotpassword()">\n\n        <ion-icon name="md-key"></ion-icon>\n\n        {{"forgot" | translate }}\n\n      </button>\n\n      <button ion-button icon-left clear small color=extralight (tap)="toSingUp()">\n\n        <ion-icon name="md-person-add"></ion-icon>\n\n      {{"new_member" | translate }}\n\n      </button>\n\n    </div>\n\n  </div>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\login\login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_account_service_account_service__["a" /* AccountServiceProvider */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 837:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AboutPage = /** @class */ (function () {
    function AboutPage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    AboutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about',template:/*ion-inline-start:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\about\about.html"*/'<ion-header>\n\n  <ion-navbar>\n\n    <ion-title>\n\n      About\n\n    </ion-title>\n\n  </ion-navbar>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\about\about.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */]])
    ], AboutPage);
    return AboutPage;
}());

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 96:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_account_service_account_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__login_login__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__appointmentCreate_appointmentCreate__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ProfilePage = /** @class */ (function () {
    function ProfilePage(navCtrl, navParams, viewCtrl, accountService, loadingCtrl, alertCtrl, nav) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.accountService = accountService;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.nav = nav;
        this.name = "";
        this.email = "";
        this.promos = [];
        this.promosArray = [];
        this.promosList = [];
    }
    ProfilePage.prototype.loadUserCredentialsStorage = function () {
        var username = window.localStorage.getItem('user');
        return username;
    };
    ProfilePage.prototype.ionViewWillEnter = function () {
        this.presentLoading();
    };
    ProfilePage.prototype.presentLoading = function () {
        var loader = this.loadingCtrl.create({
            content: "Please wait....",
            duration: 100
        });
        //after charge, get the promos and intialize the list 
        loader.present();
        this.username = this.loadUserCredentialsStorage();
        this.promosList = this.promosArray;
        this.getPromos();
    };
    ProfilePage.prototype.initializePromos = function () {
        this.promos = this.promosList;
    };
    ProfilePage.prototype.setPromo = function (promo) {
        var _this = this;
        this.navCtrl.parent.select(1).then(function () {
            _this.navCtrl.parent.getSelected().push(__WEBPACK_IMPORTED_MODULE_4__appointmentCreate_appointmentCreate__["a" /* AppointmentPage */], { promo: promo });
        });
    };
    ProfilePage.prototype.getPromos = function () {
        var _this = this;
        return new Promise(function (resolve) {
            var promosArray = [];
            _this.accountService.getPromosService()
                .subscribe(function (data) {
                var promociones = data.json();
                if (promociones.length != 0) {
                    for (var _i = 0, promociones_1 = promociones; _i < promociones_1.length; _i++) {
                        var i = promociones_1[_i];
                        promosArray.push(i);
                    }
                    _this.promosList = promosArray;
                    _this.initializePromos();
                }
            });
            resolve(true);
        });
    };
    ProfilePage.prototype.initializeUser = function () {
        this.users = this.user;
    };
    ProfilePage.prototype.logout = function () {
        // this.menuCtrl.close();
        this.loaderOut();
        this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_3__login_login__["a" /* LoginPage */]);
    };
    ProfilePage.prototype.loaderOut = function () {
        var loader = this.loadingCtrl.create({
            content: "Please wait....",
            duration: 100
        });
        //after charge, get the promos and intialize the list 
        loader.present();
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-profile',template:/*ion-inline-start:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\profile\profile.html"*/'<ion-header>\n\n  <ion-toolbar color="primary">\n\n    <ion-buttons left menuToggle>\n\n      <button ion-button icon-only>\n\n        <ion-icon name="menu"></ion-icon>\n\n      </button>\n\n    </ion-buttons>\n\n    <ion-title>{{"profile" | translate }}</ion-title>\n\n  </ion-toolbar>\n\n</ion-header>\n\n<ion-content scroll="false">\n\n<div class="card-background-page">\n\n  <ion-card>\n\n    <img src="assets/imgs/cover.jpg"/>\n\n    <div class="bg_curve"></div>\n\n  </ion-card>\n\n\n\n    <h2>¡Hola,{{username}}!</h2>\n\n\n\n  <h1>{{"promocion" | translate }}</h1>\n\n    <div>\n\n      <ion-list>\n\n        <ion-item>\n\n          <ion-label (click)="setPromo(15)">\n\n            <ion-icon name="ios-pricetag-outline" size="small" slot="start"></ion-icon>\n\n            15% DTO - Cambio 4 ruedas\n\n          </ion-label>      \n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-label (click)="setPromo(10)">\n\n            <ion-icon name="ios-pricetag-outline" size="small" slot="start"></ion-icon>\n\n            10% DTO - Cambio de aceite\n\n          </ion-label>      \n\n        </ion-item>\n\n        <ion-item>\n\n          <ion-label (click)="setPromo(10)">\n\n            <ion-icon name="ios-pricetag-outline" size="small" slot="start"></ion-icon>\n\n            10% DTO - Puesta a punto ITV\n\n          </ion-label>      \n\n        </ion-item>\n\n      </ion-list>\n\n    </div>\n\n  </div>\n\n      <ion-list padding no-margin>\n\n        <ion-item>\n\n          <ion-label style="color:#b7b7b7;font-size:14px;text-align: center;">\n\n            <ion-icon name="ios-information-circle-outline" size="small" slot="start"></ion-icon>\n\n            {{"information" | translate }}\n\n          </ion-label>      \n\n        </ion-item>\n\n      </ion-list>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\Noelia\Desktop\PacoDasPuchas\ionicapp\src\pages\profile\profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_account_service_account_service__["a" /* AccountServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Nav */]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ })

},[511]);
//# sourceMappingURL=main.js.map